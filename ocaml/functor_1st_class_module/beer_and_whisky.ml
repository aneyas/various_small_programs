module Beer = struct
    type t = BEER
    let pour () = 
        let () = Printf.printf "... a nice head ..." in BEER;;

    let consume t = Printf.printf "Ha! Nothing like a good beer to quency the thirst\n"
end

module Whisky = struct
    type t = WHISKY 
    let pour () = 
        let () = Printf.printf "... 2 fingers of this ...." in WHISKY;;

    let consume _ = Printf.printf "... Ha! Piss of the Gods!\n"
end

module type Beverage = sig
    type t 
    val pour : unit -> t 
    val consume : t -> unit
end

module Alkie = functor (B:Beverage) -> struct
    let rec another = function
        | 0 -> ()
        | i -> let b = B.pour () in
        let () = B.consume b in
        another (i-1)
end

let menu = Hashtbl.create 3;;
let () = Hashtbl.add menu "Beer" (module Beer:Beverage);;
let () = Hashtbl.add menu "Whisky" (module Whisky:Beverage);;

    let favourite = ref "Whisky" in
    let usage = "usage::" in
    let () = Arg.parse [("--beverage", Arg.Set_string favourite, Printf.sprintf "which beverage to use (%s)" !favourite)]
    (fun s -> raise (Arg.Bad s)) 
    usage in
    let x = Hashtbl.find menu !favourite in
    let module B =  (val x : Beverage) in
    let module AlkieB = Alkie(B) in
    AlkieB.another 3;;
