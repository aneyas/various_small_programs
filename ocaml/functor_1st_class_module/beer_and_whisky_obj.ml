class type beverage = object
    method consume:unit -> unit
end

class beer = object (self:#beverage)
    method consume () = Printf.printf "...drank beer\n"
end

let make_beer () =
    let () = Printf.printf "pouring beer\n" in
    let b = new beer in
    (b :> beverage) (*b是beverage的子对象,函数返回的类型是beverage *)

class whisky = object(self : #beverage)
    method consume () = Printf.printf "...drank whisky\n"
end

let make_whisky () =
    let () = Printf.printf "pouring whisky\n" in
    let b = new whisky in
    (b :> beverage)


class alkie p = object (self)
    method another n =
        if n = 0
        then ()
        else
            let b = p () in
            let () = b#consume () in
            self#another (n-1)
end


let menu = Hashtbl.create 3
let () = Hashtbl.add menu "Beer" make_beer
let () = Hashtbl.add menu "Whisky" make_whisky

let () =
    let favourite = ref "Whisky" in
    let () = Arg.parse
    [("--beverage", Arg.Set_string favourite,Printf.sprintf "which beverage to use (%s)" !favourite)]
    (fun s -> raise (Arg.Bad s))
    ("usage:")
    in
    let p = Hashtbl.find menu !favourite in
    let a = new alkie p in (* 注入对象 *)
    a#another 3 ;;
