open Core.Std;;

let fib_norec fib i = 
    if i <=1 then i
    else fib (i - 1) + fib (i - 2)
;;

(** Combine with the following, we have a recursive knot*)
let rec fib i = fib_norec fib i;;

let make_rec f_norec =
    let rec f x = f_norec f x in
    f
;;

let fib2 = make_rec fib_norec;;

(** Now memorize the result to accelerate*)

let memorize f =
    let table  = Hashtbl.Poly.create () in
    (fun x ->
        match Hashtbl.find table x with
        | Some y -> y
        | None ->
                let y = f x in
                Hashtbl.add_exn table ~key:x ~data:y;
                y
    )
;;

let memo_rec f_norec x =
    let fref = ref (fun _ -> assert false) in
    let f = memorize (fun x -> f_norec !fref x) in
    fref := f;
    f x
;; (*Note without x, the function wouldn't run*)

let fib3 = memo_rec fib_norec;;

(** Compare how fast fib3 is to fib2*)

