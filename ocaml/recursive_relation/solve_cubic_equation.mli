(*a b c d are the coefficients of a*x^3+b*x^2+c*x+d=0 *)
(*val solve_cubic_equation : float float float float -> float*)

val find_root : float -> float -> float -> float
val set_coef : float -> float -> float -> float -> unit
val set_xrange : float -> float -> unit
