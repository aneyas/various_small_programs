(*
  Dawei Wang
  Recursive relation.
  Main module
*)

open Printf
open Scanf
open Core
open Batteries
open Array

let alpha = ref 1.0
let beta = ref 0.4
let gamma = ref 0.05
let n = ref 100
let input_file = "input.in"

let read_real chan = Scanf.bscanf chan "%f " (fun x -> x);;
let difference a b = Array.fold_left (+.) 0.0 (Array.map2 (fun x y ->
  abs_float ( x -. y)) a b);;

let read_parameter a b g n =
  let ic = open_in input_file in
  let chan= Scanf.Scanning.from_channel ic in
  let nn = Scanf.bscanf chan "%d " (fun x -> x) in
  let aa = read_real chan in
  let bb = read_real chan in
  let gg = read_real chan in
  begin
    n := nn; a := aa; b := bb; g := gg; close_in ic
  end
;;

let debug_my () =
  print_endline "test";
  printf "%d\n" !n;
  printf "%f\n" !alpha;
  printf "%f\n" !beta;
  printf "%f\n" !gamma
;;

let self_consistent ()=
  let u = Array.init (!n+1) (fun i -> 0.0) in
  (*Init the last two elements*)
  let u0 = sqrt (!gamma +. !alpha)/.(2.0 *. !beta) in
  Array.fill u 0 (!n) u0;
  u.(0) <- 0.0; u.(!n) <- 0.0;

  (*For elements between 1 and !n-1, solve the equation -2*alpha*u_i +
    4*beta*u_i^3=gamma*(u_(i+1) + u_(i-1)) *)
  (*Prepare for the equations*)
  
  Solve_cubic_equation.set_xrange 0.0 10.0;

  let sol uu = 
    for i = 1 to !n - 1 do
      let () =Solve_cubic_equation.set_coef (4.0*.(!beta)) 0.0 (
          (~-.2.0)*.(!alpha)) ((~-. !gamma)*.(uu.(i+1) +. uu.(i-1))) in
      let solution = Solve_cubic_equation.find_root 1e-8  1e-8   1e-6     in
      uu.(i) <- solution
    done;
  in  let v = Array.copy u in 
  let () = sol u in
  let count = ref 0 in
  while  (difference u v) > 1e-6 && (!count < 40) do
    Array.iteri (fun i x -> v.(i)<-u.(i)) v;
    sol u;
    incr count;
    Utility.print_array u
  done;
  printf "%s: %d\n" "Total number of iterations: "  !count;
;;

let main() =
  read_parameter alpha beta gamma n;
 (* debug_my (); *)
  self_consistent ();
;;

main();;
