type coef = {mutable a1 : float; mutable a2 : float; mutable a3 :
    float; mutable a4 : float};;

type x_range = {mutable x_min : float; mutable x_max : float};;

let default_coef = {a1=1.0; a2=1.0; a3=1.0; a4=1.0};;
let default_x_range = {x_min = -10.0; x_max=10.0};;

let f x = default_coef.a1*.x*.x*.x +.
  default_coef.a2*.x*.x +. default_coef.a3*.x +. default_coef.a4;;

let df x = default_coef.a1 *. 3.0 *. x *. x +. default_coef.a2 *. 2.0
                                               *. x +. default_coef.a3 *. x;;

(* PUBLIC *)
(*a b c d are the coefficients of a*x^3+b*x^2+c*x+d=0 *)
let find_root epsabs epsrel epsf =  
  Solve.bounded_newton ~epsabs:epsabs ~epsrel:epsrel ~epsf:epsf
  default_x_range.x_min default_x_range.x_max f df 
;;

let set_coef a1 a2 a3 a4 =
  default_coef.a1 <- a1;
  default_coef.a2 <- a2;
  default_coef.a3 <- a3;
  default_coef.a4 <- a4
;;

let set_xrange x1 x2 =
  default_x_range.x_min <-x1;
  default_x_range.x_max <- x2
;;
