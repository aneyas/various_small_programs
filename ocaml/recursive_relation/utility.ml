let print_array u = 
  Array.iteri (fun i x -> print_int i; print_string ", ";print_float x; print_endline "") u;
  print_endline "------";
;;

(*

  let deno = Array.map (fun x -> (-2.0)*.(!alpha) +. 4.0*.(!beta)*.x*.x) u in 
  u.(0) <- abs_float (!gamma *. u.(1) /. deno.(0) );
  for i = 1 to !n - 2 do
    u.(i) <- abs_float (!gamma*. ( u.(i+1) +. u.(i-1) ) /. deno.(i) )
  done;

  u.(1) <- -2.0 *. !alpha
  
  Array.iter (fun x -> print_float x; print_endline "") u
*)
