type token =
  | INT of (int)
  | PLUS
  | MINUS
  | TIMES
  | OPEN
  | CLOSE
  | EOF

val expr1 :
  (Lexing.lexbuf  -> token) -> Lexing.lexbuf -> expr
