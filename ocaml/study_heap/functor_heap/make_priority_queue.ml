open Priority_queue_sig
  
(* Type for ordered comparisons.*)
type comparison = LessThan | Equal | GreaterThan

(* Signature for ordered objects. *)
module type OrderedSig =
sig
  type t
  val compare: t -> t -> comparison
end

module MakePriorityQueue (Elt : OrderedSig)
  : (PriorityQueueSig with type t = Elt.t) =
struct
  exception Empty

  type t = Elt.t

  type queue =  Leaf | Node of  t * int * queue * queue

  let empty ()= Leaf

  let is_empty q = match q with
      Leaf -> true
    | Node _ -> false

  let rec merge q1 q2 = 
    match (q1,q2) with
    | (Leaf, _) -> q2
    | (_, Leaf) -> q1
    | (Node (c1,_,lnode,rnode1), Node (c2,_,_,_))
      ->begin
          match Elt.compare c1 c2 with
          |LessThan -> make_queue c1 lnode (merge rnode1 q2) 
          |_ -> merge q2 q1
        end
  and  make_queue c lnode rnode =
    match (lnode, rnode) with
    |(Leaf,Leaf) -> Leaf
    |(Leaf,_) -> make_queue c rnode Leaf
    |(Node _ as node1, Leaf) -> Node (c,1,node1,Leaf) 
    |(Node (_,r1,_,_), Node (_,r2,_,_))
      ->begin
          match  r1< r2 with
          |true -> make_queue c rnode lnode
          |_ -> Node (c,r2+1,lnode,rnode)
        end
  let find_min q = match q with
    |Leaf -> raise Empty
    |Node (c,_,_,_) -> c
      
  let delete_min q = match q with
    |Leaf -> raise Empty
    |Node (_,_,lnode,rnode) -> merge lnode rnode

  let insert q c = 
    let q1 = Node (c,1,Leaf,Leaf) in
    merge q q1
      
  let rec from_list l = match l with
    | [] -> empty ()
    | hd::tl -> insert (from_list tl) hd
                  
  (*For testing purpose*)
  let get_lnode q = match q with
    |Leaf -> empty ()
    |Node (_,_,lnode,rnode) -> lnode
      
end 
