module type PriorityQueueSig =
    sig
        exception Empty

        type t
        type queue

        val empty : unit-> queue

        val is_empty : queue -> bool
	val find_min : queue -> t
        val delete_min: queue -> queue   
        val insert: queue->t->queue
        val from_list : t list -> queue
        val merge : queue->queue->queue
	(*For testing purpose*)
	val get_lnode : queue->queue
    end


