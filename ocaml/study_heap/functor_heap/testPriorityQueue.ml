open Make_priority_queue

module OrderedString =
  struct
    type t = string
    let compare x y = 
      if x = y then Equal else if x < y then LessThan else GreaterThan
  end

module StringPQ=MakePriorityQueue(OrderedString)

let lstring = ["aau"; "cd"; "elf"; "boy"; "zoe"; "bad"; "good";"zfe"];;
let q = StringPQ.from_list lstring;;

let px = ref q;;
while (not (StringPQ.is_empty !px)) 
do
  print_string (StringPQ.find_min !px);
  print_endline "";
  px := StringPQ.delete_min !px;
done;;
