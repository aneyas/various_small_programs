open Priority_queue_sig;;

module PriorityQueue : (PriorityQueueSig with type t = int ) =
    struct
        exception Empty

        type t = int
        type queue =  Leaf | Node of int * int * queue * queue

        let empty ()= Leaf

        let is_empty q = match q with
            Leaf -> true
          | Node _ -> false

        let rec merge q1 q2 = 
          match (q1,q2) with
            | (Leaf, _) -> q2
            | (_, Leaf) -> q1
            | (Node (c1,_,lnode,rnode1), Node (c2,_,_,_))
              -> if c1 > c2 then merge q2 q1 
                 else  make_queue c1 lnode (merge rnode1 q2) 
	and  make_queue c lnode rnode =
		match (lnode, rnode) with
		|(Leaf,Leaf) -> Leaf
		|(Leaf,_) -> make_queue c rnode Leaf
		|(Node _ as node1, Leaf) -> Node (c,1,node1,Leaf) 
		|(Node (_,r1,_,_), Node (_,r2,_,_))
		 -> if r1 < r2  then make_queue c rnode lnode
		    else Node (c,r2+1,lnode,rnode)
  
	let find_min q = match q with
	  |Leaf -> raise Empty
	  |Node (c,_,_,_) -> c

        let delete_min q = match q with
          |Leaf -> raise Empty
          |Node (_,_,lnode,rnode) -> merge lnode rnode

        let insert xxq c = 
          let q1 = Node (c,1,Leaf,Leaf) in
	  merge q q1

	let rec from_list l = match l with
	  | [] -> empty ()
	  | hd::tl -> insert (from_list tl) hd

	(*For testing purpose*)
	let get_lnode q = match q with
	  |Leaf -> empty ()
	  |Node (_,_,lnode,rnode) -> lnode

   end

(*  Look here if necessary.
 *  http://stackoverflow.com/questions/15260430/how-to-implement-a-binary-heap-using-list-in-ocaml*)
