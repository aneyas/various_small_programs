open PriorityQueue

let lnum = [-5;1;4;2;10;5;6;7];;

let q = PriorityQueue.from_list lnum;;

(*Test insert*)
let q1 = PriorityQueue.empty ();;
let q2 = PriorityQueue.insert q 110;;
let q3 = PriorityQueue.insert q2 (-1);;

print_int (PriorityQueue.find_min q3);;
print_endline "";;
let q4 = PriorityQueue.delete_min q3;;
print_int (PriorityQueue.find_min q4);;
print_endline "";;
let q5 = PriorityQueue.delete_min q4;;
print_int (PriorityQueue.find_min q5);;
print_endline "";;

(*Test merge
let p1 = PriorityQueue.empty ();;
let p2 = PriorityQueue.merge (PriorityQueue.insert p1 3) q3;;
print_int (PriorityQueue.find_min (PriorityQueue.get_lnode p2));;
print_endline "";;
 *)


print_endline "--";;
let px = ref q3;;
while (not (PriorityQueue.is_empty !px)) 
do
  print_int (PriorityQueue.find_min !px);
  print_endline "";
  px := PriorityQueue.delete_min !px;
done;;
