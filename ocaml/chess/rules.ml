(*
  Tomek Czajka
  Chess
  Rules module
*)

(* PUBLIC *)
type color = White | Black;;

let opposite = function White->Black | Black->White;;


(* PUBLIC *)
type piece_type = Pawn | Rook | Knight | Bishop | Queen | King;;

(* PUBLIC *)
type piece = piece_type * color;;


(* PUBLIC abstract type (immutable) *)
(*
  (arr, to_move, castle_rights, en_passant)
  arr: array [0..63] of piece option, (x,y) is at [8*y+x]
  to_move: color to move
  castle_rights: ((white short castle possible,white long)
                  (black short,black long))
  en_passant: in which column capture en_passant is possible, -1=none
*)
type position = piece option array * color * ((bool*bool)*(bool*bool)) * int;;


(* PUBLIC *)
type move =
  | SimpleMove of (int*int)*(int*int)
  | Capture    of (int*int)*(int*int)
  | EnPassant  of (int*int)*(int*int)
  | Promotion  of (int*int)*(int*int)*piece_type
  | CapturePromotion of (int*int)*(int*int)*piece_type
  | Castle of (int*int)*(int*int)*(int*int)*(int*int)
;;

(* PUBLIC *)
type result = Stalemate | Checkmate of color

let in_bounds (x,y) = x>=0 && x<8 && y>=0 && y<8;;

(* PUBLIC *)
let all_squares =
  let a = Utils.seq 0 7 in
  List.rev (
    List.fold_left (fun acc1 x ->
      List.fold_left (fun acc2 y -> (x,y) :: acc2)  acc1 a
    ) [] a
  )
;;

(* PUBLIC *)
let initial_position =
  let row1 = [Rook;Knight;Bishop;Queen;King;Bishop;Knight;Rook] in
  let row2 = [Pawn;Pawn;Pawn;Pawn;Pawn;Pawn;Pawn;Pawn] in
  let mid =  [None;None;None;None;None;None;None;None] in
  let f c = List.map (fun x -> Some (x,c)) in
  let all = List.concat
    [f White row1; f White row2; mid;mid;mid;mid; f Black row2; f Black row1]
  in
  let arr = Array.of_list all in
  (arr, White, ((true,true),(true,true)), -1)  
;;

let initial_position2 =
  let arr = Array.make 64 None in
  arr.(63) <- Some(King,Black);
  arr.(0) <- Some(King,White);
  arr.(1) <- Some(Queen,White);
  (arr, White, ((false,false),(false,false)), -1)  
;;


let piece_at_arr arr (x,y) = arr.(8*y+x);;

(* PUBLIC *)
(* piece_at: position -> int*int -> piece option *)
let piece_at (arr,_,_,_) = piece_at_arr arr;;

(* PUBLIC *)
let current_player (_,to_move,_,_) = to_move;;

(* PUBLIC *)
let can_castle (_,_,(w,b),_) = function White->w | Black->b ;;

(* PUBLIC *)
let en_passant (_,_,_,ep) = ep;;


(* PUBLIC *)
let move_squares = function
  | SimpleMove (a,b) | Capture (a,b) | EnPassant (a,b) | Promotion (a,b,_)
  | CapturePromotion (a,b,_) | Castle (a,b,_,_)
    -> (a,b)
;;

(* PUBLIC *)
let move_piece_type pos m =
  match piece_at pos (fst (move_squares m)) with
    | None -> failwith "Rules.move_piece"
    | Some(p,_) -> p
;;

let string_of_square (x,y) =
  let res = String.make 2 ' ' in
  res.[0]<- Char.chr (Char.code 'a' + x);
  res.[1]<- Char.chr (Char.code '1' + y);
  res
;;

let string_of_piece_type = function Pawn->"" | Rook->"R" | Knight->"N"
  | Bishop->"B" | Queen->"Q" | King->"K"
;;

(* PUBLIC *)
let string_of_move pos m =
  let (sq1,sq2) = move_squares m in
  let ssq1 = string_of_square sq1 in
  let ssq2 = string_of_square sq2 in
  let sp = match piece_at pos sq1 with
    | None -> failwith "Rules.string_of_move"
    | Some(p,_) -> string_of_piece_type p
  in
  match m with
    | SimpleMove _ -> sp ^ ssq1 ^ "-" ^ ssq2
    | Capture _ | EnPassant _ -> sp ^ ssq1 ^ "x" ^ ssq2
    | Promotion (_,_,x) -> sp ^ ssq1 ^ "-" ^ ssq2 ^ string_of_piece_type x
    | CapturePromotion (_,_,x) ->
          sp ^ ssq1 ^ "x" ^ ssq2 ^ string_of_piece_type x
    | Castle _ -> if fst sq2 > fst sq1 then "O-O" else "O-O-O"
;;

let rook_dirs = [(1,0);(0,1);(-1,0);(0,-1)];;
let knight_dirs = [(1,2);(2,1);(-1,2);(-2,1);(1,-2);(2,-1);(-1,-2);(-2,-1)];;
let bishop_dirs = [(1,1);(-1,1);(1,-1);(-1,-1)];;
let queen_dirs = rook_dirs @ bishop_dirs;;
let king_dirs = queen_dirs;;


let make_move_arr arr m =
  let arr2 = Array.copy arr in
  begin
    match m with
      | SimpleMove ((x,y),(x2,y2)) | Capture ((x,y),(x2,y2)) ->
          arr2.(8*y2+x2) <- arr2.(8*y+x);
          arr2.(8*y+x) <- None
      | EnPassant ((x,y),(x2,y2)) ->
          arr2.(8*y2+x2) <- arr2.(8*y+x);
          arr2.(8*y+x) <- None;
          arr2.(8*y+x2) <- None
      | Promotion ((x,y),(x2,y2),p) | CapturePromotion ((x,y),(x2,y2),p) ->
          let c = match arr2.(8*y+x) with
            | Some (_,cc) -> cc
            | None -> failwith "make_move ??"
          in
          arr2.(8*y+x) <- None;
          arr2.(8*y2+x2) <- Some (p,c)
      | Castle ((x1,y1),(x2,y2),(x3,y3),(x4,y4)) ->
          arr2.(8*y2+x2) <- arr2.(8*y1+x1);
          arr2.(8*y1+x1) <- None;
          arr2.(8*y4+x4) <- arr2.(8*y3+x3);
          arr2.(8*y3+x3) <- None
  end;
  arr2
;;

let square_in_check_arr arr col (x,y) =
  let opp = opposite col in
  let pawn_dirs = match col with
    | White -> [(-1,1);(1,1)]
    | Black -> [(-1,-1);(1,-1)]
  in
  let check_short dirs p =
    List.fold_left (fun acc (dx,dy) -> acc || (
      let (x2,y2) = (x+dx,y+dy) in
      (in_bounds (x2,y2) && piece_at_arr arr (x2,y2) = Some (p,opp) )
    )) false dirs
  in
  let look_dir (dx,dy) =
    let rec f (xx,yy) =
      if in_bounds (xx,yy) then
        match piece_at_arr arr (xx,yy) with
          | Some a -> Some a
          | None -> f (xx+dx, yy+dy)
      else None
    in
      f (x+dx, y+dy)
  in
  let check_long dirs p =
    List.fold_left (fun acc (dx,dy) -> acc ||
      look_dir (dx,dy) = Some (p,opp)
    ) false dirs
  in
  (
    check_short pawn_dirs   Pawn   ||
    check_short king_dirs   King   ||
    check_short knight_dirs Knight ||
    check_long  rook_dirs   Rook   ||
    check_long  bishop_dirs Bishop ||
    check_long  queen_dirs  Queen
  )
;;


let in_check_arr arr col =
  let k = Utils.array_find arr (Some(King,col)) in
  square_in_check_arr arr col (k mod 8, k/8)
;;

(* PUBLIC *)
let in_check (arr, to_move, _, _) = in_check_arr arr to_move;;

let try_move arr to_move m =
  let arr2 = make_move_arr arr m in
  if not (in_check_arr arr2 to_move) then [m] else []
;;

(* PUBLIC *)
(* generate_captures: position -> move list *)
let generate_captures (arr, to_move, _, en_passant) =
  let tr = try_move arr to_move in
  let opp = opposite to_move in
  let pawn_captures (x,y) =
    let dy = (match to_move with White -> 1 | Black -> -1) in
    let y2 = y+dy in
    if(y2>=0 && y2<8) then begin
      let promotion = y2=0 || y2=7 in
      List.fold_left (fun acc x2 ->
        if x2>=0 && x2<8 then begin
          match piece_at_arr arr (x2,y2) with
            | None ->
                if x2=en_passant && (y=4 && dy=1 || y=3 && dy = -1) then
                  tr (EnPassant ((x,y), (x2,y2)) ) @ acc
                else acc
            | Some (_, c) when c=opp ->
                if promotion then
                  List.fold_left (fun acc2 p->
                    tr (CapturePromotion ((x,y),(x2,y2),p) ) @ acc2
                  ) acc [Queen; Rook; Bishop; Knight]
                else
                  tr (Capture ((x,y),(x2,y2)) )  @ acc
            | _ -> acc
        end
        else acc
      ) [] [x-1; x+1];
    end else []
  in
  let short_captures (x,y) = List.fold_left (fun acc (dx,dy) ->
    let (x2,y2) = (x+dx, y+dy) in
    if in_bounds (x2,y2) then begin
      match piece_at_arr arr (x2,y2) with
        | Some (_, c) when c=opp -> tr (Capture ((x,y),(x2,y2)) ) @ acc
        | _ -> acc
    end else acc
  ) []
  in

  let long_captures (x,y) = List.fold_left (fun acc (dx,dy) ->
    let rec f (xx,yy) =
      if in_bounds (xx,yy) then begin
        match piece_at_arr arr (xx,yy) with
          | None -> f (xx+dx, yy+dy)
          | Some (_, c) when c=opp -> tr (Capture ((x,y),(xx,yy)) )
          | _ -> []
      end
      else []
    in f (x+dx, y+dy) @ acc
  ) []
  in

  List.fold_left (fun acc sq ->
    (
      match piece_at_arr arr sq with
        | Some (Pawn, c) when c=to_move -> pawn_captures sq
        | Some (Rook, c) when c=to_move -> long_captures sq rook_dirs
        | Some (Knight, c) when c=to_move -> short_captures sq knight_dirs
        | Some (Bishop, c) when c=to_move -> long_captures sq bishop_dirs
        | Some (Queen, c) when c=to_move -> long_captures sq queen_dirs
        | Some (King, c) when c=to_move -> short_captures sq king_dirs
        | _ -> []
    ) @ acc
  ) [] all_squares
;;

let generate_non_captures (arr, to_move, castle_rights, _) =
  let tr = try_move arr to_move in
  let opp = opposite to_move in
  
  let pawn_moves (x,y) =
    let dy = (match to_move with White -> 1 | Black -> -1) in
    let y2 = y+dy in
    let promotion = y2=0 || y2=7 in
    if y2>=0 && y2<8 && piece_at_arr arr (x,y2) = None then begin
      begin
        (* move by 1 *)
        if promotion then
          List.fold_left (fun acc p ->
            tr (Promotion ((x,y),(x,y2),p) ) @ acc
          ) [] [Queen; Rook; Bishop; Knight]
        else
          tr (SimpleMove ((x,y),(x,y2)) )
      end
      @
      begin
        (* move by 2 *)
        if((y=1 && dy=1 || y=6 && dy = -1)
            && piece_at_arr arr (x,y+2*dy) = None) then
          tr (SimpleMove ((x,y),(x,y+2*dy)) )
        else []
      end
    end else []
  in
 
  let short_moves (x,y) = List.fold_left (fun acc (dx,dy) ->
    let (x2,y2) = (x+dx, y+dy) in
    if in_bounds (x2,y2) then begin
      match piece_at_arr arr (x2,y2) with
        | None -> tr (SimpleMove ((x,y),(x2,y2)) ) @ acc
        | _ -> acc
    end else acc
  ) [] in

  let long_moves (x,y) = List.fold_left (fun acc (dx,dy) ->
    let rec f acc (xx,yy) =
      if in_bounds (xx,yy) then begin
        match piece_at_arr arr (xx,yy) with
          | None -> f (tr (SimpleMove ((x,y),(xx,yy)) ) @ acc) (xx+dx, yy+dy)
          | _ -> acc
      end else acc
    in f acc (x+dx, y+dy)
  ) [] in

  let castles =
    let (y,cr) = match to_move with
      | White -> (0,fst castle_rights)
      | Black -> (7,snd castle_rights)
    in
    begin
      if fst cr &&
         piece_at_arr arr (5,y) = None &&
         piece_at_arr arr (6,y) = None &&
         not (square_in_check_arr arr to_move (4,y)) &&
         not (square_in_check_arr arr to_move (5,y)) &&
         not (square_in_check_arr arr to_move (6,y)) then
           tr (Castle ((4,y),(6,y),(7,y),(5,y)) )
      else []
    end
    @
    begin
      if snd cr &&  
         piece_at_arr arr (1,y) = None && 
         piece_at_arr arr (2,y) = None &&
         piece_at_arr arr (3,y) = None &&
         not (square_in_check_arr arr to_move (2,y)) &&
         not (square_in_check_arr arr to_move (3,y)) &&
         not (square_in_check_arr arr to_move (4,y)) then
           tr (Castle ((4,y),(2,y),(0,y),(3,y)) )
      else []
    end
  in
  
  List.fold_left (fun acc sq ->
    (
      match piece_at_arr arr sq with
        | Some (Pawn, c) when c=to_move -> pawn_moves sq
        | Some (Rook, c) when c=to_move -> long_moves sq rook_dirs
        | Some (Knight, c) when c=to_move -> short_moves sq knight_dirs
        | Some (Bishop, c) when c=to_move -> long_moves sq bishop_dirs
        | Some (Queen, c) when c=to_move -> long_moves sq queen_dirs
        | Some (King, c) when c=to_move -> short_moves sq king_dirs
        | _ -> []
    ) @ acc
  ) castles all_squares
;; (* generate_non_captures *)

(* PUBLIC *)
let generate_moves p = generate_captures p @ generate_non_captures p;;

(* PUBLIC *)
let is_capture = function
  | SimpleMove _ | Castle _ | Promotion _ -> false
  | Capture _ | CapturePromotion _ | EnPassant _ -> true
;;

(* PUBLIC *)
let generate_captures pos =
  List.filter is_capture (generate_moves pos)
;;


(* PUBLIC *)
(* make_move: position -> move -> position *)
let make_move (arr, to_move, ((cws,cwl),(cbs,cbl)), en_passant) m =
  let arr2 = make_move_arr arr m in
  let to_move2 = opposite to_move in
  let (sq1, sq2) = move_squares m in
  let castle_rights2 = (
    (cws && sq1<>(4,0) && sq2<>(4,0) && sq1<>(7,0) && sq2<>(7,0),
     cwl && sq1<>(4,0) && sq2<>(4,0) && sq1<>(0,0) && sq2<>(0,0)),
    (cbs && sq1<>(4,7) && sq2<>(4,7) && sq1<>(7,7) && sq2<>(7,7),
     cbl && sq1<>(4,7) && sq2<>(4,7) && sq1<>(0,7) && sq2<>(0,7))
  ) in
  let en_passant2 = begin
    match m with
      | SimpleMove ((x1,y1),(x2,y2)) when (
          abs (y2-y1) = 2 &&
          piece_at_arr arr (x1,y1) = Some (Pawn, to_move)
        ) ->
            let f x =
              x>=0 && x<8 &&
              piece_at_arr arr (x,y2) = Some (Pawn, to_move2) &&
              let arr3 = make_move_arr arr2 (EnPassant ((x,y2),(x1,(y1+y2)/2))) in
              not (in_check_arr arr3 to_move2)
            in
              if f (x1-1) || f (x1+1) then x1 else -1
      | _ -> -1
  end in
  (arr2, to_move2, castle_rights2, en_passant2)
;;


(* PUBLIC *)
let outcome (arr, to_move, _, _) =
  if in_check_arr arr to_move then Checkmate (opposite to_move)
  else Stalemate
;;
