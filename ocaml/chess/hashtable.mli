(*
  Tomek Czajka
  Chess
  Hash table (forgetting, colisions are ignored)
*)

module type HASHER = sig
  type key
  type key_id (* unique key identifier type with operator "=" *)
  
  val hash: key -> int
  val id: key -> key_id
end

module Hashtable :
  functor (Hasher: HASHER) -> sig
    type key = Hasher.key
    type key_id = Hasher.key_id
    type 'a hashtable
    type hash_t

    val create: int -> 'a hashtable (* size *)
    val hash: key -> hash_t
    val find_hash: 'a hashtable -> hash_t -> 'a option
    val insert_hash: 'a hashtable -> hash_t -> 'a -> unit
    val find: 'a hashtable -> key -> 'a option
    val insert: 'a hashtable -> key -> 'a -> unit
  end
