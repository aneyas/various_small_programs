(*
  Tomek Czajka
  Chess
  Position hasher
*)

module Hasher: Hashtable.HASHER with
  type key = Rules.position and type key_id = int*int*int
