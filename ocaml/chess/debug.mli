(*
  Tomek Czajka
  Chess
  Debugging
*)

(* turn on debug output *)
val turn_on: unit -> unit

(* print a line *)
val print: string -> unit
