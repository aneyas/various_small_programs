(*
  Tomek Czajka
  Chess
  AI module
*)

(* 128K positions * about 100b = about 12.8MB *)
let hashtable_size = 128 * 1024;;

let max_depth = 100;;
  
module PosHashtable = Hashtable.Hashtable(Pos_hash.Hasher);;

(* value = (depth, best move option, lowerbound, upperbound) *)
(* key+key_id+value = about 100b *)
let hashtable = PosHashtable.create hashtable_size;;

let dummy_timer() = false;;
let timer = ref dummy_timer;;

let stats_positions = ref 0;;
let stats_in_hashtable = ref 0;;

exception Time_up;;

let evaluate_end_of_game pos =
  match Rules.outcome pos with
    | Rules.Stalemate -> 0
    | Rules.Checkmate col ->
          if col = Rules.current_player pos then
            Evaluator.win
          else -Evaluator.win
;;

(* return (value, move option) *)
(* if depth>0 and alpha=-inf and beta=+inf and a move possible - don't return None *)
(* seen = list of seen position hashes, with pos included *)
let rec alpha_beta pos depth alpha beta seen =
  if !timer() then raise Time_up;
  incr stats_positions;
  let hash = List.hd seen in
  let (depth_in_hashtable, best_move, lowerbound, upperbound) =
    match PosHashtable.find_hash hashtable hash with
      | None -> (-1, None, -Evaluator.win, Evaluator.win-1)
      | Some (d2, m, lb, ub) ->
          incr stats_in_hashtable;
          if d2 >= depth then (d2, m, lb, ub)
          else (d2, m, -Evaluator.win, Evaluator.win-1)
  in
  if lowerbound >= upperbound || lowerbound >= beta then (lowerbound,best_move)
  else if upperbound<=alpha then (upperbound, best_move)
  else begin
    let only_captures = depth=0 && not (Rules.in_check pos) in
    let result1 = max lowerbound
      (if only_captures then Evaluator.evaluate pos else -Evaluator.inf)
    in
    let result2 = 
      if result1>=beta then result1
      else match best_move with
        | None -> result1
        | Some m -> if not only_captures || Rules.is_capture m then
                      max result1
                      (try_move pos depth (max alpha result1) beta seen m)
                    else result1
    in
    let (moves, end_of_game) =
      if result2 >= beta then ([], false)
      else if only_captures then (Rules.generate_captures pos, false)
      else
        let mm = Rules.generate_moves pos in
        (mm, mm=[])
    in
    if end_of_game then (evaluate_end_of_game pos, None)
    else begin
      let (result,res_move) = List.fold_left (fun (res, rm) m ->
        if res<beta && best_move <> Some m then
          let v = try_move pos depth (max alpha res) beta seen m in
          if v > res then (v, Some m) else (res, rm)
        else (res, rm)
      ) (result2, best_move) moves in

      if depth >= depth_in_hashtable then begin
          PosHashtable.insert_hash hashtable hash (depth, res_move,
            (if result>alpha then result else result1),
            (if result<beta then result else upperbound)
          )
      end;
      
      (result, res_move)
    end
  end   

and try_move pos depth alpha beta seen m =
  let expand v dv =
    if v < -(Evaluator.win/2) then v-dv
    else if v > Evaluator.win/2 then v+dv
    else v
  in
    let pos2 = Rules.make_move pos m in
    let hash = PosHashtable.hash pos2 in
    if List.mem hash seen then 0
    else begin
      let seen2 =
        if Rules.is_capture m || Rules.move_piece_type pos m = Rules.Pawn
        then [hash] else hash::seen
      in
      let (v,_) = alpha_beta pos2 (max (depth-1) 0)
            (expand (-beta) 1) (expand (-alpha) 1) seen2 in
      - (expand v (-1))
    end
;;


(* PUBLIC *)
let choose_move time state =
  let my_timer = Utils.timer time in
  let pos = Game_state.current_position state in
  let seen = List.map PosHashtable.hash (Utils.take
    (Game_state.non_pawn_moves state + 1) (Game_state.seen_positions state))
  in
  let best_move = ref (List.hd (Rules.generate_moves pos)) in
  let value = ref 0 in
  stats_positions := 0;
  stats_in_hashtable := 0;
  begin
    timer := dummy_timer;
    try
      for d=1 to max_depth do
        if d = 2 then timer := my_timer;
        let (v, m) = alpha_beta pos d (-Evaluator.win + 2) (Evaluator.win-1) seen in
        match m with
          | None -> failwith "Brain.choose_move"
          | Some mm -> best_move := mm
        ;
        value := v;
        Debug.print ("Depth " ^ string_of_int d ^ ": " ^
          Rules.string_of_move pos !best_move ^ " " ^ string_of_int v);
      done
    with Time_up -> ()
  end;
  Debug.print ("Positions: "^string_of_int !stats_positions^
    ", in hash table: "^string_of_int (!stats_in_hashtable * 100 /
    !stats_positions)^"%");
  !best_move
;;

