(*
  Tomek Czajka
  Chess
  Position evaluator
*)

(* value of immediate win *)
val win: int

(* infinite value *)
val inf: int

(* evaluate a position (big = good for the current player) *)
val evaluate: Rules.position -> int
