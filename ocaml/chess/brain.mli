(*
  Tomek Czajka
  Chess
  AI module
*)

(* choose best move given time to think (in seconds) and game state *)
val choose_move: int -> Game_state.state -> Rules.move
