(*
  Tomek Czajka
  Chess
  Interface
*)

let human_color = ref Rules.White;;

let init_string = " 420x460";;
let font = "*courier-bold-r-normal--24*";;
let square_size = (50, 50);;
let chessboard_offset = (10, 50);;
let string_offset = (10, 10);;
let string_max_size = (400, 40);;

let black_square_color = Graphics.rgb 64 128 64;;
let white_square_color = Graphics.rgb 192 192 0;;
let dark_black_square_color = Graphics.rgb 0 96 0;;
let dark_white_square_color = Graphics.rgb 96 96 0;;
let black_piece_color = Graphics.rgb 0 0 0;;
let white_piece_color = Graphics.rgb 255 255 255;;
let highlight_color = Graphics.rgb 255 255 128;;
let highlight_width = 3;;

let position = ref Rules.initial_position;;


let show_string s =
  Graphics.set_color Graphics.background;
  Graphics.fill_rect (fst string_offset) (snd string_offset)
               (fst string_max_size) (snd string_max_size);
  Graphics.set_color Graphics.foreground;
  let (xx,yy) = Graphics.text_size s in
  Graphics.moveto (fst string_offset + (fst string_max_size - xx)/2)
                  (snd string_offset + (snd string_max_size - yy)/2);
  Graphics.draw_string s
;;

let square_color (x,y) dark =
  if (x+y) mod 2 = 0 then
    if dark then dark_black_square_color else black_square_color
  else
    if dark then dark_white_square_color else white_square_color
;;

let piece_color = function
  | Rules.White -> white_piece_color
  | Rules.Black -> black_piece_color
;;

let piece_shape =
  let sym l = l @ (List.rev_map (fun (x,y)->(100-x,y)) l) in
  let p = sym [(20,10);(20,90)] in
  let pawn = sym
    [(20,10);(23,23);(40,40);(30,47);(30,55);(45,70);
    (40,75);(40,80);(45,85)]
  and rook = sym
    [(20,10);(20,20);(30,20);(30,75);(20,85);(20,90);
     (32,90);(32,80);(40,80);(40,90)]
  and knight =
    [(20,10);(90,10);(85,50);(60,84);(45,88);(45,98);
     (37,88);(30,98);(30,88);(10,40);(15,35);(48,55);
     (40,30)]
  and bishop = sym
    [(50,13);(10, 8);( 8,12);(45,21);(27,26);
     (37,40);(30,50);(26,60);(30,68);(47,87);
     (42,93);(50,98)]
  and queen = sym
    [(25,10);(10,85);(30,50);(25,92);(40,60);(50,95)]
  and king = sym
    [(25,10);(20,30);(10,60);(20,70);(35,70);(45,60);
     (45,75);(35,75);(35,85);(45,85);(45,95)]
  in function
  | Rules.Pawn -> pawn | Rules.Rook -> rook | Rules.Knight -> knight
  | Rules.Bishop -> bishop | Rules.Queen -> queen | Rules.King -> king
;;

let square_position (x,y) =
  let (ox,oy) = chessboard_offset in
  let (sx,sy) = square_size in
  match !human_color with
    | Rules.White -> (ox+x*sx, oy+y*sy)
    | Rules.Black -> (ox+(7-x)*sx, oy+(7-y)*sy)
;;

let highlight_square sq =
  let (x,y) = square_position sq in
  let (sx,sy) = square_size in
  Graphics.set_color highlight_color;
  Graphics.set_line_width highlight_width;
  Graphics.draw_rect x y sx sy
;;
  
let draw_square (xx,yy) color piece =
  let (w,h) = square_size in
  Graphics.set_color color;
  Graphics.fill_rect xx yy w h;
  match piece with
    | None -> ()
    | Some (p,c) ->
        let poly = List.map (
          function (x,y) -> (x*(w-1)/100 + xx, y*(h-1)/100 + yy)
        ) (piece_shape p) in
        Graphics.set_color (piece_color c);
        Graphics.fill_poly (Array.of_list poly)
;;

let draw_position pos dark =
  Graphics.auto_synchronize false;
  Graphics.clear_graph();
  List.iter (function sq->
    draw_square (square_position sq) (square_color sq (List.mem sq dark))
      (Rules.piece_at pos sq)
  ) Rules.all_squares;
  Graphics.synchronize();
  Graphics.auto_synchronize true
;;

(* PUBLIC *)
let init hc =
  human_color := hc;
  Graphics.open_graph init_string;
  Graphics.set_font font;
  Graphics.set_window_title Utils.program_name;
  draw_position !position []
;;

(* PUBLIC *)
let destroy() =
  Graphics.close_graph()
;;

let string_of_color = function
  | Rules.White -> "White"
  | Rules.Black -> "Black"
;;

(* PUBLIC *)
let make_move m =
  let sq = Rules.move_squares m in
  position := Rules.make_move !position m;
  draw_position !position [fst sq; snd sq]

(* unit -> (int,int) *)  
let mouse_click() =
  let st = Graphics.wait_next_event [Graphics.Button_down] in
  (st.Graphics.mouse_x, st.Graphics.mouse_y)
;;

(* PUBLIC *)
let wait() = ignore (mouse_click());;

let choose_promotion col =
  let (ox,oy) = chessboard_offset in
  let (sx,sy) = square_size in
  let pieces = [Rules.Queen; Rules.Rook; Rules.Bishop; Rules.Knight] in
  Graphics.set_color Graphics.background;
  Graphics.fill_rect (ox+sx) (oy+3*sy) (6*sx) (2*sy);
  let pos i = (ox + sx + sx/4 + i * (sx*3/2), oy + sy*7/2) in
  for i=0 to 3 do
    draw_square (pos i) black_square_color (Some(List.nth pieces i, col))
  done;
  let rec nr() =
    let (mx,my) = mouse_click() in
    let ok i =
      let(x,y)=pos i in
      mx >= x && mx < x+sx && my >= y && my <= y+sy
    in
    if List.exists ok [0;1;2;3] then
      List.find ok [0;1;2;3]
    else nr()
  in
  List.nth pieces (nr())
;;

(* PUBLIC *)
let choose_move() =
  let to_move = Rules.current_player !position in
  show_string ("To move: " ^ string_of_color to_move ^ " (human)");
  let move_list = Rules.generate_moves !position in
  
  let rec square_click ok = (* f - is square ok *)
    let (x1,y1) = mouse_click() in
    let (ox,oy) = chessboard_offset in
    let (sx,sy) = square_size in
    let (x2,y2) = (x1 - ox, y1 - oy) in
    if x2>=0 && x2<8*sx && y2>=0 && y2<8*sy then
      let res = match !human_color with
        | Rules.White -> (x2/sx, y2/sy)
        | Rules.Black -> (7-x2/sx, 7-y2/sy)
      in
      if ok res then res else square_click ok
    else square_click ok
  in

  let sq1 = square_click (fun sq ->
    List.exists (fun m -> fst (Rules.move_squares m) = sq) move_list
  ) in
  highlight_square sq1;
  let sq2 = square_click (fun sq ->
    List.exists (fun m -> Rules.move_squares m = (sq1,sq)) move_list
  ) in
  highlight_square sq2;
  let m1 = List.find (fun m -> Rules.move_squares m = (sq1,sq2)) move_list in
  match m1 with
    | Rules.Promotion _ ->
        Rules.Promotion(sq1,sq2,choose_promotion to_move)
    | Rules.CapturePromotion _ ->
        Rules.CapturePromotion(sq1,sq2,choose_promotion to_move)
    | _ -> m1
;;

(* PUBLIC *)
let show_computer_thinking() =
  let to_move = Rules.current_player !position in
  show_string ("To move: " ^ string_of_color to_move ^ " (computer)")
;;

(* PUBLIC *)
let show_result = function
  | Game_state.Undecided -> show_string "UNDECIDED"
  | Game_state.Draw -> show_string "DRAW"
  | Game_state.Win Rules.White -> show_string "WHITE WINS"
  | Game_state.Win Rules.Black -> show_string "BLACK WINS"
;;
