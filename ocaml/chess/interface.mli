(*
  Tomek Czajka
  Chess
  Interface
*)

(* initialize the interface given human color *)
val init: Rules.color -> unit

(* destroy the interface *)
val destroy: unit -> unit

(* make a move *)
val make_move: Rules.move -> unit

(* choose a move *)
val choose_move: unit -> Rules.move

(* show computer is thinking *)
val show_computer_thinking: unit -> unit

(* show final result *)
val show_result: Game_state.status -> unit

(* wait at the end of game *)
val wait: unit -> unit
