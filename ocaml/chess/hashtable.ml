(*
  Tomek Czajka
  Chess
  Hash table (forgetting) implementation
*)

module type HASHER = sig
  type key
  type key_id (* unique key identifier type with operator "=" *)
  
  val hash: key -> int
  val id: key -> key_id
end

module Hashtable =
  functor (Hasher: HASHER) ->
  struct
    type key = Hasher.key;;
    type key_id = Hasher.key_id;;
    type 'a hashtable = (key_id * 'a) option array;;
    type hash_t = int * key_id;;
    
    let create size = Array.make size None;;
    
    let hash k = (Hasher.hash k, Hasher.id k);;
    
    let find_hash ht (h, id) =
      match ht.(abs(h mod Array.length ht)) with
        | None -> None
        | Some (kid, v) -> if id = kid then Some v else None
    ;;

    let insert_hash ht (h, id) v =
      ht.(abs(h mod Array.length ht)) <- Some (id, v)
    ;;

    let find ht k = find_hash ht (hash k);;
    let insert ht k v = insert_hash ht (hash k) v;;
  end
