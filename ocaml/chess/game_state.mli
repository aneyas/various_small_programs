(*
  Tomek Czajka
  Chess
  Game state
*)

(* game state - abstract type *)
type state

type status = Undecided | Draw | Win of Rules.color

val initial: state

val game_status: state -> status

val current_position: state -> Rules.position

(* all seen positions, current position first *)
val seen_positions: state -> Rules.position list

val non_pawn_moves: state -> int

val make_move: state -> Rules.move -> state
