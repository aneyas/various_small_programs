(*
  Tomek Czajka
  Chess
  Game state
*)

type status = Undecided | Draw | Win of Rules.color;;

(* PUBLIC abstract type *)
(* (position list, moves without capture or pawn move *)
type state = Rules.position list * int;;

(* PUBLIC *)
let initial = ([Rules.initial_position], 0);;

(* PUBLIC *)
let current_position (l, _) = List.hd l;;

(* PUBLIC *)
let seen_positions (l, _) = l;;

(* PUBLIC *)
let non_pawn_moves (_,x) = x;;

(* PUBLIC *)
let game_status ((l,c) as st) =
  let p = current_position st in
  if c >= 100 then Draw
  else if Utils.list_count p l >= 3 then Draw
  else if Rules.generate_moves p <> [] then Undecided
  else match Rules.outcome p with
    | Rules.Stalemate -> Draw
    | Rules.Checkmate col -> Win col
;;

(* PUBLIC *)
let make_move ((l,c) as st) m =
  let p = current_position st in
  let p2 = Rules.make_move p m in
  let c2 = match m with
    | Rules.SimpleMove (sq,_) -> begin
        match Rules.piece_at p sq with
          | Some(Rules.Pawn,_) -> 0
          | _ -> c+1
      end
    | Rules.Capture _ | Rules.EnPassant _ | Rules.Promotion _
    | Rules.CapturePromotion _ -> 0
    | Rules.Castle _ ->  c+1
  in
  (p2::l, c2)
;;
