(*
  Tomek Czajka
  Chess
  Utilities
*)

(* PUBLIC *)
let program_name = "Chess v1.1";;

(* PUBLIC *)
let list_count a =
  List.fold_left (fun acc x -> if x=a then acc+1 else acc) 0
;;

(* PUBLIC *)
let rec seq a b = if a>b then [] else a :: seq (a+1) b;;

(* PUBLIC *)
let rec take n l =
  if n = 0 then []
  else match l with
    | [] -> []
    | h::t -> h::take (n-1) t
;;

(* PUBLIC *)
let array_find arr x =
  let rec f i =
    if i = Array.length arr then raise Not_found
    else if arr.(i) = x then i
    else f (i+1)
  in f 0
;;

(* PUBLIC *)
let list_find_index l a =
  let rec f l i =
    match l with
      | [] -> raise Not_found
      | h::t -> if h=a then i else f t (i+1)
  in f l 0
;;

Random.self_init();;

(* PUBLIC *) 
let random = Random.int;;
let random_bits = Random.bits;;

(* PUBLIC *)
let timer t =
  let limit = Sys.time() +. float_of_int t in
  let cnt = ref 0 in
  function () -> begin
    incr cnt;
    if !cnt = 100 then begin
      cnt := 0;
      Sys.time() >= limit
    end
    else false
  end
;;
