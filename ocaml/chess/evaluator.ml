(*
  Tomek Czajka
  Chess
  Position evaluator
*)

(* PUBLIC *)
let win = 1000000;;

(* PUBLIC *)
let inf = 2000000;;

let center (x,y) = (7 - max (abs (2*x-7) ) (abs (2*y-7) )) / 2;; (*0,1,2,3*)

(* PUBLIC *)
let evaluate pos =
  let to_move = Rules.current_player pos in
  List.fold_left (fun acc (x,y) ->
    let add v c =
      if c = to_move then acc+v else acc-v
    in
    match Rules.piece_at pos (x,y) with
      | None -> acc
      | Some(Rules.King,c) -> add (center(x,y)) c
      | Some(Rules.Pawn,c) ->
          let (st,dy) =
            match c with Rules.White->(0,1) | Rules.Black->(7,-1)
          in
          add (85 + (y-st)/dy * 5 + 3 * center (x,y) ) c
      | Some(Rules.Rook,c) -> add 450 c
      | Some(Rules.Knight,c) ->
          add ( 300 + 10 * center (x,y) ) c
      | Some(Rules.Bishop,c) ->
          add ( 310 + 10 * center (x,y) ) c
      | Some(Rules.Queen,c) ->
          add ( 900 + 5 * center (x,y) ) c
  ) 0 Rules.all_squares
;;
