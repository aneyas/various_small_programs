(*
  Tomek Czajka
  Chess
  Utilities
*)

(* name that shows as the window name *)
val program_name: string

(* count elements in a list *)
val list_count: 'a -> 'a list -> int

(* list [a..b] *)
val seq: int -> int -> int list

(* take n first elements *)
val take: int -> 'a list -> 'a list

(* raise Not_found if not found *)
val array_find: 'a array -> 'a -> int

(* raise Not_found if not found *)
val list_find_index: 'a list -> 'a -> int

(* random number generator, [0..x) *)
val random: int -> int

(* random bits generator, [0..2^30) *)
val random_bits: unit -> int

(* given number of seconds, returns a timer:
   function returning true when time is up *)
val timer: int -> unit -> bool
