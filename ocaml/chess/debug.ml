(*
  Tomek Czajka
  Chess
  Debugging
*)

let on = ref false;;

(* PUBLIC *)
let turn_on() = on := true;;

(* PUBLIC *)
let print s =
  if !on then prerr_endline s
;;
