(*
  Tomek Czajka
  Chess
  Main module
*)

let human_color = ref Rules.White;;
let time_per_move = ref 5;;

let state = ref Game_state.initial;;

let intro() =
  print_string (Utils.program_name ^ " by Tomek Czajka\n");
  print_string ("Possible arguments: white, black, <time per move (sec)>, debug\n");
  print_string ("Press CTRL-C to exit\n");
  flush stdout
;;

exception Invalid_program_argument of string;;

let parse_arguments() =
  let args = List.tl (Array.to_list Sys.argv) in
  List.iter (function
  | "white" -> human_color := Rules.White
  | "black" -> human_color := Rules.Black
  | "debug" -> Debug.turn_on()
  | x -> begin
           try
             time_per_move := int_of_string x;
             if !time_per_move <= 0 then raise (Invalid_program_argument x)
           with _ -> raise (Invalid_program_argument x)
         end
  ) args
;;

let play() =
  while Game_state.game_status !state = Game_state.Undecided do
    let p = Game_state.current_position !state in
    let m =
      if Rules.current_player p = !human_color then
        Interface.choose_move()
      else begin
        Interface.show_computer_thinking();
        Brain.choose_move !time_per_move !state
      end
    in
    Interface.make_move m;
    let p2 = Rules.make_move p m in
    state := Game_state.make_move !state m
  done;
  Interface.show_result (Game_state.game_status !state)
;;

let main() =
  try
    parse_arguments();
    intro();
    Interface.init !human_color;
    play();
    while true do Interface.wait() done;
    Interface.destroy()
  with
  | Invalid_program_argument s ->
      print_string ("Invalid program argument: "^s^"\n")
  | Graphics.Graphic_failure _ -> () (* window closed probably *)
;;

main();;
