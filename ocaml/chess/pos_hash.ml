(*
  Tomek Czajka
  Chess
  Position hasher
*)

let piece_type_nr = function Rules.Pawn->0 | Rules.Rook->1 | Rules.Knight->2
  | Rules.Bishop->3 | Rules.Queen->4 | Rules.King->5;;

let color_nr = function Rules.White->0 | Rules.Black->1;;

(* make_hash_fun: unit -> position -> int *)
let make_hash_fun() =
  let bits = Array.make (64*12+1+4+8) 0 in
  for i=0 to Array.length bits - 1 do
    bits.(i) <- Utils.random_bits()
  done;
  function pos ->
    let res = ref 0 in
    for x=0 to 7 do
      for y=0 to 7 do
        match Rules.piece_at pos (x,y) with
          | None -> ()
          | Some (p,c) ->
              res := !res lxor bits.((8*y+x)*12 + 2 * piece_type_nr p + color_nr c)
      done
    done;
    if Rules.current_player pos = Rules.Black then
      res := !res lxor bits.(64*12);
    let (ws,wl) = Rules.can_castle pos Rules.White in
    if ws then res := !res lxor bits.(64*12+1);
    if wl then res := !res lxor bits.(64*12+2);
    let (bs,bl) = Rules.can_castle pos Rules.Black in
    if bs then res := !res lxor bits.(64*12+3);
    if bl then res := !res lxor bits.(64*12+4);
    let en_passant = Rules.en_passant pos in
    if en_passant <> -1 then res := !res lxor bits.(64*12+5+en_passant);
    !res
;;


(* PUBLIC *)
module Hasher: Hashtable.HASHER
     with type key = Rules.position and type key_id = int*int*int = struct
  type key = Rules.position;;
  type key_id = int*int*int;;

  let hash = make_hash_fun();;
  
  let id =
    let (f1,f2,f3) = (make_hash_fun(),make_hash_fun(),make_hash_fun()) in
    function p -> (f1 p, f2 p, f3 p)
  ;;
  
end
