(*
  Tomek Czajka
  Chess
  Rules module
*)

type color = White | Black

type piece_type = Pawn | Rook | Knight | Bishop | Queen | King

type piece = piece_type * color

(* abstract (immutable) type of chess positions *)
type position

type move =
  | SimpleMove of (int*int)*(int*int)
  | Capture    of (int*int)*(int*int)
  | EnPassant  of (int*int)*(int*int)
  | Promotion  of (int*int)*(int*int)*piece_type
  | CapturePromotion of (int*int)*(int*int)*piece_type
  | Castle of (int*int)*(int*int)*(int*int)*(int*int)

type result = Stalemate | Checkmate of color

(* all squares on the chessboard *)
val all_squares: (int*int) list

val initial_position: position

val piece_at: position -> int*int -> piece option
val current_player: position -> color
(* (short castle, long castle) *)
val can_castle: position -> color -> bool*bool
(* column in which capture en-passant is possible, -1 if none *)
val en_passant: position -> int

(* move_squares: move -> (from, to) *)
val move_squares: move -> (int*int)*(int*int)

val move_piece_type: position -> move -> piece_type

val string_of_move: position -> move -> string

val is_capture: move -> bool

(* check for check *)
val in_check: position -> bool

(* generate just captures *)
val generate_captures: position -> move list

(* empty list = end of game (checkmate or stalemate) *)
val generate_moves: position -> move list

val make_move: position -> move -> position

(* can be only called for end-of-game positions (generate_moves p = []) *)
val outcome: position -> result
