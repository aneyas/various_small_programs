open Core
open Printf

let rec accumulate m l =
  match m with
   0 -> []
  | _ -> accumulate (m-1) l@[int_of_string Sys.argv.(m)]

let read_input () =
  let n = Array.length Sys.argv - 1 in
  begin
    ( accumulate n [] )
  end

let int_list =  read_input ()

(* DEBUG
List.iter (fun x -> print_int x; print_endline "" ) int_list;;
*)

(* Defination of last*)
let rec last  = function
  | [] -> None
  | [x] -> Some x
  | _ :: rest -> last rest

let x = last int_list 

let  run x = 
  match x with
  | Some y -> print_int y; print_endline ""
  | None -> print_endline ""

let () = run x

(* last_two *)

let rec last_two = function
  | [] -> None
  | x :: [y] -> Some x
  | _ :: rest -> last_two rest

let y = last_two int_list

let () = run y

(* Defination of at; Q3*)
let rec at k ll = 
  if (k <= 0) then None
  else
  begin
    match ll with
  | [] -> None
  | a :: rest -> if (k==1) then Some a else at (k-1) rest
  end

let at_3  = at 3 int_list

let () = run (at 4 int_list)



