{
    open Mem_parser
    let line = ref 1
}

    let digit = [ '0'-'9']
    let mantissa = digit+ '.' digit* | digit* '.' digit+
    let exponent = [ 'e' 'E' ] [ '+' '-'] digit+
    let floating = ['+' '-']? mantissa exponent?
    let integer = ['+' '-']? digit+

    rule token = parse
    | [' ' '\t'] {token lexbuf}
    | '\n' {incr line; token lexbuf}
    | '{'  {OPEN}
    | '}'  {CLOSE}
    | ','  {COMMA}
    | floating | integer {FLOAT (float_of_string (Lexing.lexeme lexbuf)) }
    | eof  {EOF}
    |_     {failwith ("Miskate in line input")}
