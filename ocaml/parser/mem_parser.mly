%token EOF OPEN CLOSE COMMA
%token <float> FLOAT

%start main
%type <float list> main

%%

tail:
    |FLOAT COMMA tail {$1 :: $3}
    |FLOAT CLOSE {[$1]};

main:
    |OPEN tail EOF { $2 }
    |OPEN CLOSE EOF { [] };
