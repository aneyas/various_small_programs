open Core.Std;;

let a = Mem_parser.main Mem_lexer.token (Lexing.from_channel stdin);;
let consts = Array.of_list a;;

Array.iter ~f:(fun x-> printf "%f\n" x) consts;;
