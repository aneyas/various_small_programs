open Scanf;;
open String;;
open Printf;;
open Num;;

let n = Scanf.scanf "%d " (fun x -> x)
let a= Array.make n 0

let () = 
  for i = 0 to n-1 do
    let j = Scanf.scanf "%d " (fun x -> x) in
    a.(i) <- j
  done
;;

(*
let () = 
  Array.map (fun x -> printf "%d " x) a;
  print_endline " "
  *)

let m = Scanf.scanf "%d " (fun x -> x)
let b = Array.make m 0
let () = 
  for i = 0 to m-1 do
    let j = Scanf.scanf "%d " (fun x -> x) in
    b.(i) <- j
  done
;;

module ST: sig
  type t
  val make : int array -> t
  val traverse : t -> (int -> unit) -> unit
  val update : t -> int -> int -> t
  val hit : t -> int  -> t
end = struct
  type t = 
    |Leaf of int
    |Node of int *  t * t

  let tval x = 
    match x with 
    |Leaf z -> z
    |Node (z,_,_) -> z

  let make aa = 
    let rec make1 s e =
      if s = e then
        Leaf aa.(s)
      else
        begin
          let mid = (s+e)/2 in
          let l = make1 s mid in
          let r = make1 (mid+1) e in
          Node (max (tval l) (tval r), l, r)
        end
     in make1 0 ((Array.length aa) - 1)

  let rec traverse st f = 
    match st with 
    | Leaf x -> f x
    | Node (x,l,r) -> 
        f x; traverse l f; traverse r f

  let update st idx x =
    let rec update' st' s e =
      if idx >= s && idx <= e then
            match st' with
            | Leaf _ -> a.(s)<-x; Leaf x
            | Node (_,l,r)  ->
                begin 
                  let mid = (s+e)/2 in
                  let ll = update' l s mid in
                  let rr = update' r (mid+1) e in
                  Node ((max (tval ll) (tval rr)), ll, rr)
                end
      else st'
    in update' st 0 ((Array.length a)-1)

  let hit st x =
    let rec hit' st' s e =
      match st' with
            | Leaf v ->
                begin
                if v > x then
                  let tmp = a.(s) -1 in 
                  begin
                  a.(s) <- tmp; Leaf tmp
                  end
                else st'
                end
            | Node (v,l,r)  ->
                if v > x then
                  let mid = (s+e)/2 in
                  let ll = hit' l s mid in
                  let rr = hit' r (mid+1) e in
                  Node ((max (tval ll) (tval rr)), ll, rr)
                else st'
     in 
     hit' st 0 ((Array.length a)-1)

end;;


let st = ST.make a

(*let () = Array.map (fun x -> printf "%d " x) a; print_endline " "*)

(*let () = ST.traverse st (fun x -> printf "%d\n" x)*)
let rec loop i stx = 
  match i with
  | _ as j when j = m -> Array.iter (fun x -> printf "%d " x) a
  | _ -> loop (i+1) (ST.hit stx b.(i))
in loop 0 st
