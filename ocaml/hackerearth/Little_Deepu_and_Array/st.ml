type 'a st = 
  |Leaf of 'a
  |Node of 'a * 'a st * 'a st
