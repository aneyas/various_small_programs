open Scanf;;
open String;;
open Printf;;
open Num;;

let n = Scanf.scanf "%d " (fun x -> x)
let a= Array.make n 0

let () = 
  for i = 0 to n-1 do
    let j = Scanf.scanf "%d " (fun x -> x) in
    a.(i) <- j
  done
;;

(*
let () = 
  Array.map (fun x -> printf "%d " x) a;
  print_endline " "
  *)

let m = Scanf.scanf "%d " (fun x -> x)
let b = Array.make m 0
let () = 
  for i = 0 to m-1 do
    let j = Scanf.scanf "%d " (fun x -> x) in
    b.(i) <- j
  done
;;

module ST: sig
  type t
  val make : int array -> t
  val hit : t -> int  -> unit

end = struct
  type t = 
    |Leaf of int ref * int (*The second int is index*)
    |Node of int ref * t * t

  let tval x = 
    match x with 
    |Leaf (z,_) -> !z
    |Node (z,_,_) -> !z

  let make aa = 
    let rec make1 s e =
      if s = e then
        Leaf ((ref aa.(s)), s)
      else
        begin
          let mid = (s+e)/2 in
          let l = make1 s mid in
          let r = make1 (mid+1) e in
          Node (ref (max (tval l) (tval r)), l, r)
        end
     in make1 0 ((Array.length aa) - 1)

  let hit st x =
    let rec hit' st' =
      let z = tval st' in
      if z <= x then ()
      else 
      match st' with
            | Leaf (v,i) ->
                begin
                if !v > x then
                  let tmp = a.(i) -1 in 
                  begin
                    a.(i) <- tmp; v:=tmp; ()
                  end
                else ()
                end
            | Node (v,l,r)  ->
                if !v > x then
                  let ll = hit' l in
                  let rr = hit' r in
                  v := !v-1
                else ()
     in 
     hit' st

end;;


let st = ST.make a

let () =
  for i = 0 to m-1 do
    ST.hit st b.(i)
  done; Array.iter (fun x -> printf "%d " x) a

