open Scanf;;
open String;;
open Printf;;
open Num;;

let n = Scanf.scanf "%d " (fun x -> x)
let a0= Array.make n (0,0)

let () = 
  for i = 0 to n-1 do
    let j = Scanf.scanf "%d " (fun x -> (i,x)) in
    a0.(i) <- j
  done
;;

let () = Array.sort (fun (_,x) (_,y) -> (x - y)) a0

(*let () =Array.iteri (fun i (j,x) -> printf "%d %d\n" i x) a0*)

let a = Array.map (fun (i,x) -> x) a0


(*
let () = 
  Array.map (fun x -> printf "%d " x) a;
  print_endline " "
  *)

let m = Scanf.scanf "%d " (fun x -> x)
let b = Array.make m 0
let () = 
  for i = 0 to m-1 do
    let j = Scanf.scanf "%d " (fun x -> x) in
    b.(i) <- j
  done
;;

module ST: sig
  type t
  val make : int array -> t
  val hit : t -> int  -> unit
end = struct
  type t = 
    |Leaf of int ref * int (*The second int is index*)
    |Node of int ref * int ref * int * int * t * t (*The two int are start and end*)

  let tmax x = 
    match x with 
    |Leaf (z,_) -> !z
    |Node (z,_,_,_,_,_) -> !z

  let tmin x = 
    match x with 
    |Leaf (z,_) -> !z
    |Node (_,z,_,_,_,_) -> !z

  let make aa = 
    let rec make1 s e =
      if s = e then
        Leaf ((ref aa.(s)), s)
      else
        begin
          let mid = (s+e)/2 in
          let l = make1 s mid in
          let r = make1 (mid+1) e in
          Node (ref (max (tmax l) (tmax r)), (ref (min (tmin l) (tmin r))), s,e,l, r)
        end
     in make1 0 ((Array.length aa) - 1)

  let hit st x =
    let rec mydec st' =
      match st' with 
      | Leaf (v,i) -> v:=!v-1; a.(i)<-a.(i)-1
      | Node (v,u,_,_,l,r)-> v:=!v-1; u:=!u-1; mydec l; mydec r
    in
    let rec hit' st' =
      match st' with
            | Leaf (v,i) as lf ->
                begin
                if !v > x then
                  mydec lf 
                end
            | Node (v,u,s,e,l,r) as no  ->
                if !u > x then
                  mydec no
                else 
                  begin
                    if !v <= x then
                      ()
                    else
                      begin
                        hit' l; hit' r; v:=max (tmax l) (tmax r); u:=min (tmin l) (tmin r)
                      end
                    end
     in 
     hit' st

end;;


let st = ST.make a

let () =
  (*Array.iter (fun x -> printf "%d " x) a;
  print_endline "";*)
  for i = 0 to m-1 do
    ST.hit st b.(i);
    (*printf "%d: " b.(i); Array.iter (fun x -> printf "%d " x) a; print_endline ""*)
  done
;;

let a2 = Array.make n 0

let idxf x = 
  match x with
  | (i,_) -> i

let () = 
  for i = 0 to n-1 do
    let idx = idxf(a0.(i)) in
    a2.(idx) <- a.(i)
  done

let () =  Array.iter (fun x -> printf "%d " x) a2

