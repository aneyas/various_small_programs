(* file euler001.ml *)
open Core.Std
open BatEnum      
open BatBigarray
open BatPervasives

let main () = 
    (1--999)
|> BatEnum.filter (fun i -> i mod 3 = 0 || i mod 5 = 0)
|> BatEnum.reduce (+) 
|> BatInt.print stdout
;;

let _ = main ();;
print_endline "";

BatEnum.iter (fun i -> BatInt.print stdout i; print_endline "") (1--20);;

let a = Bigarray.Array1.create float64 c_layout 10;;
Array1.set a 1 0.2;;
printf "%f\n" (Array1.get a 1);;
