open Core.Std;;

(** Note the`*)

let three = `Int 3;;
let four  = `Float 4.;;

let is_positive = function
    |`Int x -> x > 0
    |`Float x->x > 0.
;;

let xact = List.filter ~f:is_positive [three; four];;

