
open Core_extended.Std;;
open Core.Std;;

type host_info =
    {hostname : string;
     os_name : string;
     cpu_arch : string;
     timestamp : Time.t;
    };;


let my_host =
    let sh = Shell.sh_one_exn in
    { hostname = sh "hostname";
    os_name = sh "uname -s";
    cpu_arch = sh "uname -p";
    timestamp = Time.now ();
    };;

print_endline my_host.cpu_arch;;

type 'a timestamped = {item: 'a; time: Time.t};;

let first_timestamped list =
    List.reduce list ~f:(fun a b -> if a.time < b.time then a else b);;

let host_info_to_string { hostname = h; os_name = os;
timestamp = ts; cpu_arch = c } =
    sprintf "%s (%s / %s, on %s)\n" h os c (Time.to_sec_string ts);;

print_string (host_info_to_string my_host);;
print_endline;;

