module Stack =
  struct
    type 'a t = {mutable elements: 'a list}
    let create () = {elements = []}
    let push x s = s.elements <- x::s.elements
    let pop s =
      match s.elements with
      h::t -> s.elements <-t; h
      | [] -> failwith "Empty stac"
  end;;

let s = Stack.create () in
  Stack.push 1 s; Stack.push 2 s; print_int (Stack.pop s);;
