(** This is for Euler Problem 2*)
(** The final results is (a_(N+2) - a2 + 1)/2, a_N is a even number*)

open Core.Std;;


let rslt a = 
    let a1 = ref 1 in
    let a2 = ref 2 in
    let r = ref 3 in
    (
        while !a2 <= a do
            a1 := !a2;
        a2 := !r;
        r := !a1 + !a2
        done;

        if !a1 % 2 = 0 then
            (print_int ((!r - 1) / 2); print_endline "")
        else if (!a2 - !a1) % 2 = 0 then
            (print_int ((!a2 - 1) / 2); print_endline "")
        else
            print_int ((!a1 - 1) / 2); print_endline ""
            )

    (* test
let () = rslt 30;;
let () = rslt 50;;
let () = rslt 56;;
let () = rslt 89;;
let () = rslt 90;;
let () = rslt 91;;

let () = rslt 12;;
*)

let () = rslt 4000000;;

