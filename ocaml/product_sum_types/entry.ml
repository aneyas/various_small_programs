module Log_entry = struct
    type t =
        { session_id: string;
          time: Time.t;
          important: bool;
          message: string;
        }
end;;

    type client_message = 
        | Logon of Logon.t
        | Heartbeat of Heartbeat.t
        | Log_entry of Log_entry.t;;

    let message_for_user user messages =
        let (user_message, _) =
            List.fold message ~init:([],String.Set.empty)
            ~f:(fun ((messages,user_sessions) as acc) message ->
                match message with
                | Logon m ->
                        if m.Logon.user = user then
                            (message::messages, Set.add user_sessions
                            m.Logon.session_id)
                        else acc
                | Heartbeat _ | Log_entry _ ->
                        let session_id = match message with
                        | Logon m -> m.Logon.session_id
                        |

