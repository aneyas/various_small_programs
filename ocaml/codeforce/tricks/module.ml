module type Interval_intf = sig 
  type t
  type endpoint
  val create : endpoint -> endpoint -> t
  val is_empty : t -> bool
  val contains : t -> endpoint -> bool
  val intersect:t->t->t
end;;

  module type Int_interval_intf =
    Interval_intf with type endpoint = int;;
