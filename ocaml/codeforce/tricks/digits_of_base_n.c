/* Here is the essential part to calculate how many digits is needed for base-7, 
 * given a number n. */

void
x(){
  int ndd=1;
  int seven = 7;
  int rslt=7;
  while(rslt<=n-1){
    rslt +=6*seven;
    seven *=7;
    ndd++;
  }
}

/*  or more easily cei(log(m)/log(7)) */
