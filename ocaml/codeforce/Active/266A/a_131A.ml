open Scanf;;
open String;;
open Printf;;
open Int64;;
open Char;;

let s = bscanf Scanning.stdin "%s " (fun x -> x )

let to_list s =
  let l = ref [] in
  for i = 0 to (length s) - 1 do
    let x = get s i in
    l := x :: !l
  done; List.rev !l
;;

let ls = to_list s

let isu c = (uppercase c ) = c

let isl c = (lowercase c ) = c

let allu l = List.fold_right (&&) (List.map (fun x -> isu x) l) true

let pu l = match l with
  | x::[] -> isl x
  | x::tl -> (isl x) && (allu tl)

let is_to_change l = (allu l) || (pu l)

let mprint l = List.iter (fun x -> print_char x) l; print_endline ""

let change l = match l with
 | x :: [] -> (uppercase x) :: []
 | x :: tl -> (uppercase x) :: (List.map (fun y -> lowercase y) tl)

let change2 l = 
  List.map (fun x -> if isu x then lowercase x else uppercase x) l

let () =
  if is_to_change ls then
    mprint (change2 ls)
  else
    mprint ls
