open Scanf
open Printf

let n = Scanf.scanf "%d " ( fun x -> x )

let a = Array.make n (-1);;
let b = Array.make n (-1);;

let nd=181
let c = Array.make nd 0;; 

for i = 3 to 360 do
  if (360 mod i) = 0 then
    c.(180 - 360/i) <- 1
done

for i = 1 to n do
  let deg = Scanf.scanf "%d " ( fun x -> x ) in
  if c.(deg) = 0 then print_endline "NO" else print_endline "YES"
done
