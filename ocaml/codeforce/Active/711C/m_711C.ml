open Printf
open String

let (n,m,k) = Scanf.scanf "%d %d %d " (fun x y z -> (x,y,z));;

let c = Array.make n (-1) in (*color of tree*)
for i = 0 to n - 1 do
  let ci = Scanf.scanf "%d " (fun x -> x) in
  c.(i) <- ci
done

let p = Array.make_matrix n m 0 in
for i = 0 to n - 1 do
  for j = 0 to m - 1 do
    let ci = Scanf.scanf "%d " (fun x -> x) in
    p.(i).(j) <- ci
  done
done

