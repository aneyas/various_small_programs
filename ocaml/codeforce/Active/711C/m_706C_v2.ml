open Printf
open String
open Int64

let n = Scanf.scanf "%d " (fun x -> x);;

let e = Array.make n Int64.zero (*energy for reverse string*)
let s = Array.make n "" (*stores all the strings to be sorted*)
let rs = Array.make n ""

let d1 = Array.make n (Int64.max_int) (*flip*)

let d2 = Array.make n (Int64.max_int)

for i = 0 to n-1 do
  let jj = Scanf.bscanf Scanf.Scanning.stdin "%Ld " (fun x -> x) in
  e.(i) <- jj
done

let rev s =
  let len = String.length s in
  let rec rev_helper i s2 =
    if i >= len then s2
    else
      rev_helper (i+1) s2^(Char.escaped s.[i])
  in
  rev_helper 0 ""

for i = 0 to n - 1 do
  let jj = Scanf.bscanf Scanf.Scanning.stdin "%s " (fun x -> x) in
  s.(i) <- jj; rs.(i) <- (rev jj)
done

let () = d1.(0) <- of_int 0;;
let () = d2.(0) <- e.(0);;

(*printf "%Ld" max_int;;
*)

(*DEBUG*)
(*
 let () = print_endline (rev "abc")

for i = 0 to n - 1 do
  print_endline s.(i);
  print_endline rs.(i)
done
*)

let g s1 s2 =
  let x = String.compare s1 s2 in
  if (x<=0) then of_int 0
  else max_int

let f s1 s2 n =
  let x = String.compare s1 s2 in
  if (x<=0) then e.(n)
  else max_int

let my_add a b =
  if ( (compare a max_int == 0 ) || (compare b max_int == 0 ) ) then max_int
  else add a b

let () = 
  for i = 1 to n - 1 do
    let x1 = my_add d1.(i-1)  (g s.(i-1) s.(i) ) in
    let x2 = my_add d2.(i-1) (g rs.(i-1) s.(i) ) in
    d1.(i) <- min x1 x2;
   let y1 = my_add d1.(i-1) (f s.(i-1) rs.(i) i ) in
   let y2 = my_add d2.(i-1)  (f rs.(i-1) rs.(i) i ) in
   d2.(i) <- min y1 y2
done

let () =
  let x = min d1.(n-1) d2.(n-1) in
  if (x==max_int) then printf "%d\n" (-1)
  else print_endline (to_string x)

