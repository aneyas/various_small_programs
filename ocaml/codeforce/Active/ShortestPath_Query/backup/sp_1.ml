(**D. The Child and Sequence*)

open Scanf;;
open Num;;
open Printf;;
open Big_int;;

let bi i = big_int_of_int i

let n = Scanf.bscanf Scanf.Scanning.stdin "%d " ( fun x -> x);;

let imatch = Array.make n 0;;
let kmatch = ref 0;;

for i = 0 to n-1 do
  let ni = Scanf.bscanf Scanf.Scanning.stdin "%d " ( fun x -> x) in
  imatch.(i) <- ni; kmatch := ni + !kmatch
done
 ;;

printf "%d %d\n" n !kmatch

type color = Color of big_int
type length = Length of big_int
type node = Node of big_int
type edge = Edge of node * node * length * color

(*Start and end of and edge to be mappped to an edge*)
module Se = struct 
  type t = big_int * big_int
  let compare x y = 
    let (x1,x2) = x in
    let (y1,y2) = y in
    match compare_big_int x1 y1 with
    1 -> 1
   |  -1 -> -1
   | _ -> compare x2 y2
end;;

module M = struct
  module Edge_map = Map.Make(Se)
  module Node_map = Map.Make(struct type t = Big_int let compare = compare end)

  let m_edges = Edge_map.empty
  let m_nodes = Node_map.empty

  let add_data n1 n2 len color =
    begin
      let l = ref (Node_map.find n1 m_nodes) in
      l := n2 :: !l;
      m_edges = Edge_map.add (n1,n2) Edge(Node(n1),Node(n2),Length(len),Color(color)) m_edges
  end

  let node1 = Node(bi 1,Empty)
  let node2 = Node(bi 2,Empty)
  let edge1 = Edge(node1, node2, Length(bi 10), Color(bi 10) )
  let  get_edge node1 node2 = edge1
end;;

