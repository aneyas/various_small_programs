(**D. The Child and Sequence*)

open Scanf;;
open Num;;
open Printf;;
open Big_int;;

let n = Scanf.bscanf Scanf.Scanning.stdin "%d " ( fun x -> x);;

let imatch = Array.make n 0;;
let kmatch = ref 0;;

for i = 0 to n-1 do
  let ni = Scanf.bscanf Scanf.Scanning.stdin "%d " ( fun x -> x) in
  imatch.(i) <- ni; kmatch := ni + !kmatch
done
 ;;

printf "%d %d\n" n !kmatch

type color = Color of big_int

type length = Length of big_int

type edge = Edge of node * node * length * color
and node = Node of big_int * (edge list)


module type GRAPH = sig
  val get_edge : node -> node -> edge

  (* val of_adjacency : (node * node list) list -> t *)
  (*val dfs_fold : t -> node -> ('a->node->'a) ->'a->'a *)
end;;

module M : GRAPH = struct
  (*
  module Int_map = Map.Make(struct type t = Big_int let compare = compare end)
  type node = big_int 
  type t = (node list) Int_map.t
  *)

  (*
  let of_adjacency  l =
    List.fold_right ( fun (x, y) -> Char_map.add x y) l Char_map.empty
    *)

  type colors = While | Black
end;;

