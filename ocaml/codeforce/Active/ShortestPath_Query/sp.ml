open Scanf;;
open Num;;
open Printf;;
open Big_int;;

(*
let bi i = big_int_of_int i

let n = Scanf.bscanf Scanf.Scanning.stdin "%d " ( fun x -> x);;

let imatch = Array.make n 0;;
let kmatch = ref 0;;

for i = 0 to n-1 do
  let ni = Scanf.bscanf Scanf.Scanning.stdin "%d " ( fun x -> x) in
  imatch.(i) <- ni; kmatch := ni + !kmatch
done
 ;;

printf "%d %d\n" n !kmatch
*)

type edge = Edge of big_int * big_int * big_int * big_int
type se = Se of big_int * big_int

let edges : (se,edge) Hashtbl.t = Hashtbl.create 10000
let nodes : (big_int, big_int list) Hashtbl.t = Hashtbl.create 10000

let add_node n1 n2 = 
  let il = ref (Hashtbl.find nodes n1) in
  il := n2 :: !il

let add_edge n1 n2 len col =
  Hashtbl.add edges (Se(n1,n2)) (Edge(n1,n2,len,col))
