open Printf
open Num

let (n,m) = Scanf.scanf "%d %d " (fun x y -> (x,y) )

let zero = num_of_int 0

let one = num_of_int 1

let rec f x y = 
  match (x,y) with
 |(0,_) -> zero
 |(_,0) -> zero
 |(n1,m1) -> begin
   (f (n1 - 1 ) m1) +/ (f n1 (m1 - 1)) -/ (f (n1 - 1) (m1 - 1)) 
   +/ (num_of_int (m1*n1)) +/ (num_of_int (4*(m1-1)*(n1-1)))
 end

let x = f n m
in printf "%d\n" (int_of_num (mod_num x (num_of_int 1000000007)))
