#include <stdio.h>

const int N=15*10000+10;

int h[N];

int main(){
  int n, k;
  scanf("%d %d ",&n,&k);

  for(int i = 1; i<=n;i++){
    scanf("%d", &h[i]);
  }

  int min_h =0;
  int id = 1;
  for(int i = 1; i <= k; i++){
    min_h += h[i];
  }

  int len = min_h;
  for(int i = k+1; i<= n; i++){
    int new_h =len + h[i] - h[i-k];
    if(new_h < min_h){
      id = i-k+1;
      min_h = new_h;
    }
    len = new_h;
  }

  printf("%d\n", id);


  return 0;
}
