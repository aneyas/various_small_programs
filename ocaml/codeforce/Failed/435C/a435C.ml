(**D. The Child and Sequence*)

open Scanf;;
open Num;;
open Printf;;

let n = Scanf.bscanf Scanf.Scanning.stdin "%d " ( fun x -> x);;

let imatch = Array.create n 0;;
let kmatch = ref 0;;

for i = 0 to n-1 do
  let ni = Scanf.scanf "%d " ( fun x -> x) in
  imatch.(i) <- ni; kmatch := ni + !kmatch
done
 ;;

printf "%d %d\n" n !kmatch
