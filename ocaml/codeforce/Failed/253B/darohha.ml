(*amazing: this version uses the technique of two pointers*)

let chan = Scanf.Scanning.from_channel (open_in "input.txt")
let read_int () = Scanf.bscanf chan " %d " (fun x -> x)

let ochan = open_out "output.txt"
let print_string s = Printf.fprintf ochan "%s" s

let () = 
  let n = read_int() in
  let c = Array.init n (fun i -> read_int()) in
  let () = Array.sort compare c in
    
  let rec loop i j ac =
    if i=n then ac else
      if j >= n-1 || c.(j+1) > 2*c.(i) then 
          loop (i+1) j (max (j-i+1) ac)
      else loop i (j+1) ac
  in
    
  let a = loop 0 0 0 in
    print_string (Printf.sprintf "%d\n" (n-a))
