open Scanf;;
open Array;;
open Printf;;

(* This version uses binary search and rec
 *)

let file_in = "input.txt";;
let file_out = "output.txt";;

let ic = open_in file_in;;
let oc = open_out file_out;;

let n = Scanf.bscanf (Scanf.Scanning.from_channel ic) "%d " (fun x -> x);;
let a = Array.make n 0;;

let () = 
  for i = 0 to n-1 do
    let x = Scanf.bscanf (Scanf.Scanning.from_channel ic) "%d " (fun x -> x) in
    a.(i) <- x
  done in
    Array.sort (fun x y -> y - x) a
;; (*array a is from large to small*)

(*
Array.iter (fun x -> print_int x; print_string " ") a;;
print_endline "\n-------";;
 *)

close_in ic;;

let my_min x y z = min (min x y ) (min y z )

exception Found;;

let rec b1search x ii jj =
  let ai = ref jj in 
  try
    while !ai >= ii do
      if x <= 2*a.(!ai) then raise Found;
      ai:=!ai-1
    done; !ai
with Found -> !ai
;;
   
let rec b2search x ii jj =
  let ai = ref ii in 
  try
    while !ai <= jj do
      if 2*x>=a.(!ai) then raise Found;
      ai:=!ai+1
    done; !ai
with Found -> !ai
;;

(*let y = b1search 8 0 5 in
print_int y;;
 *)

let rec solve i j =
  if i >= j then 0
  else if
    a.(i) <= 2*a.(j) 
  then i+(n-1-j)
  else
    begin
      let x = j-(b1search a.(i) i j) in (*b1search search for small value*)
      let y = (b2search a.(j) i j)-i in  (*b2 search searchh for large value*)
      let z = 2 + (solve (i+1) (j-1)) in
      my_min x y z;
    end
;;

let rslt = solve 0 (n-1) 
    in
    Printf.fprintf oc "%d\n" rslt
;;
