open Scanf;;
open Array;;
open Printf;;

(*print_endline "begin";;
 *)

let file_in = "input.txt";;
let file_out = "output.txt";;

let ic = open_in file_in;;
let n = Scanf.bscanf (Scanf.Scanning.from_channel ic) "%d " (fun x -> x);;
let a = Array.make n 0;;
let ar = Array.make n 0;;
(*print_endline "xx";;
 *)
let () = 
  for i = 0 to n-1 do
    let x = Scanf.bscanf (Scanf.Scanning.from_channel ic) "%d " (fun x -> x) in
    a.(i) <- x
  done in
    Array.sort (fun x y -> y - x) a
;; (*array a is from large to small*)

for i = 0 to n-1 do
  ar.(i) <- a.(n-1-i)
done;; (*array ar is from small to large*)

(*
Array.iter (fun x -> print_int x; print_endline "") ar;;
 *)

close_in ic;;

let my_min x y = if x < y then x else y;;

exception Found;;

(*Find the start position*)
let print_int_message i x = print_string x; print_string ": "; print_int i; print_endline "";;

let rows = 
  let i = ref 0 in  
  try 
    while (!i < n) do
      if a.(0) < 2*ar.(!i) then raise Found;
      i := !i + 1 
    done; !i
  with Found -> !i;;
  
let cols = n-1 -rows;;

(*Find the end position*)

let rec find_end i j =
  if i >= n -1 || j >= n-1 then (i,j)
  else
    begin
      if ar.(i+1) <= 2*a.(j+1) then find_end (i+1) (j+1)
      else (i,j) 
    end;;

let (rowe, cole) = find_end rows cols;;

print_int_message rows "rows"; print_int_message cols "cols";;
print_int_message rows "rowe"; print_int_message cols "cole";;

let biggest = max_int;;
let nrow = rowe-rows+1;;
let ncol = cole-cols+1;;


(*
let b1 = Array.make_matrix nrow (n-cole) 0;;
for i=1 to nrow

let b = Array.make_matrix nrow ncol 0;;
(*Initialize the matrix*)
let () = 
  for i = ncol-1 downto 1 do
    b.(nrow-1).(i) <- biggest
  done;
  for i = nrow-1 downto 1 do
    b.(i).(ncol-1) <- biggest
  done
;;
 *)
(*
let () = 
  b.(nrow-1).(0) <- nrow - 1;
  b.(0).(ncol-1) <- ncol -1
;; 

let my_min x y z = min (min x y) (min y z);;

let rslt = ref (n-1) in
    print_int n; print_string " "; print_int nrow; print_string " "; print_int ncol; print_endline "";
 (*   if (nrow - 1 + ncol - 1) <n then*)
      begin
	let () = 
	  for i = ncol-2 downto 0 do
	    for j = nrow -2 downto 0 do
	      let x = b.(j+1).(i) in
	      let y = b.(j).(i+1) in
	      let z = if a.(nrow - 1 - j) > 2*a.(n-1 - (ncol-1 - i)) then biggest else (ncol-1-i)+(nrow-1-j)
	      in
	      b.(j).(i) <- (my_min x y z)
	    done
	  done in rslt:=b.(0).(0)
      end; 
    let oc = open_out file_out in
    Printf.fprintf oc "%d\n" !rslt;;
   
 *)
