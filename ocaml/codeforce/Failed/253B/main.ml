open Scanf;;
open Array;;
open Printf;;

(*print_endline "begin";;
 *)

let file_in = "input.txt";;
let file_out = "output.txt";;

let ic = open_in file_in;;
let n = Scanf.bscanf (Scanf.Scanning.from_channel ic) "%d " (fun x -> x);;
let a = Array.make n 0;;

(*print_endline "xx";;
 *)
let () = 
  for i = 0 to n-1 do
    let x = Scanf.bscanf (Scanf.Scanning.from_channel ic) "%d " (fun x -> x) in
    a.(i) <- x
  done in
    Array.sort (fun x y -> y - x) a
;;

(*Array.iter (fun x -> print_int x; print_endline "") a;;
 *)

close_in ic;;

let my_min x y = if x < y then x else y;;

exception Found;;

let nrow = 
  let row = ref 0 in
  let i = ref (n - 1) in
  try 
    while (!i >=0) do
      if a.(0) <= (2*a.(!i)) then raise Found;
      row := !row + 1;
      i:=!i-1
    done; !row+1
  with Found -> !row+1;;

let ncol = 
  let col = ref 0 in
  let i = ref 0 in
  try 
    while (!i < n) do
      if 2*a.(n-1) >= a.(!i) then raise Found;
      col := !col + 1;
      i:=!i+1
    done; !col+1
  with Found -> !col+1;;

let biggest = max_int;;
let b = Array.make_matrix nrow ncol 0;;
(*Initialize the matrix*)
let () = 
  for i = ncol-1 downto 1 do
    b.(nrow-1).(i) <- biggest
  done;
  for i = nrow-1 downto 1 do
    b.(i).(ncol-1) <- biggest
  done
;;

let () = 
  b.(nrow-1).(0) <- nrow - 1;
  b.(0).(ncol-1) <- ncol -1
;; 

let my_min x y z = min (min x y) (min y z);;

let rslt = ref (n-1) in
    print_int n; print_string " "; print_int nrow; print_string " "; print_int ncol; print_endline "";
 (*   if (nrow - 1 + ncol - 1) <n then*)
      begin
	let () = 
	  for i = ncol-2 downto 0 do
	    for j = nrow -2 downto 0 do
	      let x = b.(j+1).(i) in
	      let y = b.(j).(i+1) in
	      let z = if a.(nrow - 1 - j) > 2*a.(n-1 - (ncol-1 - i)) then biggest else (ncol-1-i)+(nrow-1-j)
	      in
	      b.(j).(i) <- (my_min x y z)
	    done
	  done in rslt:=b.(0).(0)
      end; 
    let oc = open_out file_out in
    Printf.fprintf oc "%d\n" !rslt;;
   

(*
let ncold = ncol -1 in
let nrowd = nrow -1 in
let rec find_b00 j i =
if i = ncold then
  begin
    if j = 0 then nrowd else biggest
  end
else if j = nrowd then
  begin
    if i = 0 then ncold else biggest
  end
else
  begin
    let x = find_b00 (j+1) (i) in
    let y = find_b00 (j) (i+1) in
    let z = if a.(nrow - 1 - j) > 2*a.(n-1 - (ncol-1 - i)) then biggest else (ncol-1-i)+(nrow-1-j)
    in
    my_min x y z
  end
in let rslt = find_b00 0 0 in
   let oc = open_out file_out in
   Printf.fprintf oc "%d\n" rslt;;
 *)
