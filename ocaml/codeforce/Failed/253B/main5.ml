(*amazing: this version uses the technique of two pointers*)

let chan = Scanf.Scanning.from_channel (open_in "input.txt")
let read_int () = Scanf.bscanf chan " %d " (fun x -> x)

let ochan = open_out "output.txt"
let print_string s = Printf.fprintf ochan "%s" s

let () = 
  let n = read_int() in
  let c = Array.init n (fun i -> read_int()) in
  let () = Array.sort compare c in
    
  let rec loop i j ac =
    if i=j then ac else
      if c.(j) <= 2*c.(i) then 
	loop (i+1) j (max (j-i+1) ac)
      else max (loop i (j-1) ac) (loop (i+1) j ac)
  in
    
  let a = loop 0 (n-1) 0 in
    print_string (Printf.sprintf "%d\n" (n-a))
