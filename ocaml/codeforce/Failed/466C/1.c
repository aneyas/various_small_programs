#include <stdio.h>

const int N=5*100000+10;

int a[N];

int main(){
  int n;
  scanf("%d ",&n);

  if(n<=2) {
    printf("%d\n",0);
    return 0;
  }

  long long int sum = 0;
  for(int i = 0; i< n; i++){
    scanf("%d ",&a[i]);
    sum += a[i];
  }

  long long int sum_3 = sum/3;

  //printf("%lld %lld\n", sum, sum_3);

  if(sum != sum_3 *3) {
    printf("%d\n",0);
    return 0;
  }

  long long int first_sum=0;
  int x=0;
  int ix=0;
  int flag = 0;
  for(int i=0; i<n-2; i++){
    first_sum += a[i];
    if(first_sum==sum_3){
      x++;
      flag = 1;
    }
    if(flag && first_sum != sum_3){
      ix = i-1;
      break;
    }
  }

  printf("%d %d\n", x,ix);

  long long int second_sum = 0;
  int y = 0;
  int iy = 0;
  flag = 0;
  for(int i=ix+1; i<n-1; i++){
    second_sum += a[i];
    if(second_sum==sum_3){
      y++;
      flag = 1;
    }
    if(flag && first_sum != sum_3){
      iy = i;
      break;
    }
  }

  printf("%d %d\n", y,iy);

  printf("%d\n", x*y);
  return 0;
}
