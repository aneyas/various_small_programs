open Printf

let read_int () = Scanf.scanf "%d " (fun x -> x);;

let n = Scanf.scanf "%d " (fun x -> x)

let a = Array.init n (fun i -> read_int () )

let () = Array.iter (fun x -> printf "%d " x) a; print_endline ""

let j = ref 0

let max_len = ref 1

let () =
for i = !j to n - 2 do
  let k = ref (i+1) in
  let len = ref 1 in
  while ((!k<n) && a.(!k) > a.(!k-1) ) do
    begin
      incr len;
      incr k
    end
  done;
  j := !k;
  if( !len > !max_len) then max_len := !len
done;;


let () =  printf "%d\n" !max_len;;

(*Strictly increasing numbers --> !max_len *)

let find_len s =
  begin
    (* printf "%d %d --\n" s a.(s);*)
    let len = ref 1 in
    let s1 = ref (s - 1) in
    let s2 = ref (s + 1) in
    while(!s1-1>=0 && a.(!s1)>a.(!s1-1)) do
      incr len;
      decr s1;
      (*      printf "%s %d ==\n" "In 1, Current len" !len *)
    done;
    incr len;
    while(!s2+1<n && a.(!s2)<a.(!s2+1)) do
      incr len;
      incr s2;
      (*   printf "%s %d ==\n" "In 2, Current len" !len *)
    done;
    !len+1
  end
;;


let _ =
  for i = 1 to n - 2 do
    if( (a.(i) <= a.(i-1)) && abs(a.(i-1) - a.(i+1))>1 ) then
      begin
        let lenx = find_len i in
        if(lenx > !max_len) then  (max_len := lenx)
      end
  done


let () =  printf "%d\n" !max_len;;
