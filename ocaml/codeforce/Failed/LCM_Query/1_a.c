/*
 * =====================================================================================
 *
 *       Filename:  1.c
 *
 *    Description:  The LCM query problem.
 *
 *        Version:  1.0
 *        Created:  10/03/2016 18:45:15
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  D. Wang (dw), rand_wang@163.com
 *   Organization:  XJTU
 *
 * =====================================================================================
 */

#include <stdio.h>
#include <stdlib.h>

const int N = 2000;

static long long gcd(long long a, long long b){
  if (b == 0){
    return a;
  }else{
    return gcd(b, a%b);
  }
}

static long long cal_lcm(long long a, long long b){
  return (a*b)/gcd(a,b);
}

int n,m;
int an[N];
long long int lcm[N];

static long long min_all_lcm(int x){
  // First one is a(1)...a(1+x-1) or
  // here it shall be a(0) ... a(x-1)
  long long m_min = lcm[x-1];
  long long current_lcm = lcm[x-1];
  for(int i=1; i< n-x+1;i++){
    current_lcm = cal_lcm (current_lcm,an[i+x-1]);
    current_lcm /=  an[i-1];
    //printf("--Before x: %d, i: %d, current_lcm: %lld\n",x,i,current_lcm);
    long long tmp1=-10000;
    for(int j=an[i-1]-1;j>0;j--){
      if(an[i-1]%j == 0){
        tmp1 = current_lcm * j;
        if(gcd(tmp1,an[i-1]) == j) break;
      }
    }
    if(tmp1>0){
      current_lcm = tmp1;
    }
    //printf("-- After x: %d, i: %d, current_lcm: %lld\n",x,i,current_lcm);
    if(m_min > current_lcm) m_min = tmp1;
  }
  return m_min;
}

int
main(){
  scanf("%d %d",&n,&m);

  for(int i = 0; i < n; i++){
    scanf("%d", &an[i]);
  }

  //DEBUG
//  printf("%d %d\n", n,m);
//  for(int i = 0; i < n; i++){
//    printf("%d ", an[i]);
//  }
//  printf("\n");


  lcm[0] = an[0];
  for(int i = 1; i < n; i++){
    lcm[i] = cal_lcm(lcm[i-1],an[i]);
  }
  //DEBUG
//  for(int i = 0; i < n; i++){
//    printf("%lld ", lcm[i]);
//  }
//  printf("\n");

  for(int i = 0; i < m; i++){
    //Complete the calculation.
    int q;
    scanf("%d", &q);
    printf("%lld\n", min_all_lcm(q)%(1000000000+7));
  }


  return 0;
}
