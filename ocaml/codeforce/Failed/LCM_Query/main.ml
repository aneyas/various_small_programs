open Scanf;;
open String;;
open Printf;;
open Num;;


let n m = bscanf Scanning.stdin "%d %d" (fun x y -> x y)

let modn = num_of_int 1000000007

let nn = 1000000 + 5

let a= Array.make nn (num_of_int 0)

let rec gcd a b =
    if b = 0 then a else gcd b (a mod b)

let lcm a b = (a*b) / ( gcd a b)

