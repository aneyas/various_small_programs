open Scanf;;
open String;;
open Printf;;
open Num;;

let (n, s) = bscanf Scanning.stdin "%d %d " (fun x y -> (x, y))
               
let a = Array.make n 0;;
                   
for i = 1 to n-1 do
  let  f = bscanf Scanning.stdin "%d " (fun x -> x)  in
  a.(i) <- f
done;;
     
let b=10
(* 
print_int n; print_string " "; print_int s; print_endline "";;

for i=0 to n-1 do
  print_endline (string_of_num a.(i))
done;;
*)

(*Strategy: start from the large number to small number; set two values z1_i, z2_i*)
