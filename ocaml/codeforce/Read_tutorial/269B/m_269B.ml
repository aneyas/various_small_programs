open Scanf
open Num
open Printf
open String

type plant = P of int * float;;

let (n,m) = Scanf.scanf "%d %d " (fun x y -> (x,y));;
let read_P () = Scanf.scanf "%d %f " (fun x y -> P (x, y) );;
let read_P2 () = Scanf.scanf "%d %f " (fun x y -> x );;

let plants = Array.init n (fun i -> read_P2 () );;

(* DEBUG
let printx = function
  | P(x,y) -> printf "%d %f\n" x y
in Array.iter (fun x -> print_int x; print_endline "") plants;;
*)
(* longest increasing subsequence*)
let d = Array.make n 1;;

let max_length = ref 1;;
for i = 1 to n -1 do
  for j = i - 1 downto 0 do
    if ( (plants.(i) >= plants.(j) ) && (d.(j)+1 > d.(i)) ) then (d.(i) <- d.(j) + 1)
  done;
  if(d.(i) > !max_length) then max_length := d.(i)

done

let () = print_int (n - !max_length); print_endline "";;
