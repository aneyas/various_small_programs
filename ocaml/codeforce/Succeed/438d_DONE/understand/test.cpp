/*
 * =====================================================================================
 *
 *       Filename:  test.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  06/13/2014 08:18:18 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */

#include<iostream>
#include<cstdio>
#include<cstdlib>
#include<string.h>
#include<math.h>
#include<algorithm>
#include<map>
#include<vector>
#include<queue>
using namespace std;

#define MAXN 100005

int
main(){
    cout << MAXN << " " << (MAXN<<2) << endl;
    for(int now = 1; now < 10; ++now){
        cout << now << " " << ((now<<1)|1) << ((now<<1)|1) <<endl;
    }
    cout << (2>>1) << endl;
    cout << (3>>1) << endl;

    return 0;
}
