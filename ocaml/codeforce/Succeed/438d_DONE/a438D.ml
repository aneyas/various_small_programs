(**D. The Child and Sequence*)

open Scanf;;
open Num;;
open Printf;;

let (n,m) = Scanf.bscanf Scanf.Scanning.stdin "%d %d " ( fun x y -> (x,y));;

(*Read the sequence*)
type node = {mutable l:int; mutable r:int; mutable mid:int; mutable maxx:int; mutable sum:num};;
let tree = Array.init ( 4 * n + 4 ) (fun _ -> {l=0;r=0;maxx=0; mid=0; sum = num_of_int 0});;

let rec build l r now =
    tree.(now).l <- l; tree.(now).r<-r; tree.(now).mid<- ((l+r)/2);
    if l = r then
        let x = Scanf.bscanf Scanf.Scanning.stdin "%d " ( fun x -> x) in
        tree.(now).maxx<-x;tree.(now).sum <- num_of_int x
    else
        let twonow = 2 * now in
        build l tree.(now).mid twonow;
        build (tree.(now).mid + 1) r (twonow + 1);
        tree.(now).sum <- Num.add_num tree.(twonow).sum tree.(twonow + 1).sum;
        tree.(now).maxx <- max tree.(twonow).maxx tree.(twonow + 1).maxx
in build 1 n 1 (*Note tree.(0) is not used*)

let rec query l r now = 
    if tree.(now).l=l && tree.(now).r=r then
        tree.(now).sum
    else if l > tree.(now).mid then
        query l r (2*now + 1)
    else if r <= tree.(now).mid then
        query l r (2*now)
    else
        Num.add_num (query  l tree.(now).mid (2*now)) (query (tree.(now).mid + 1) r (
            2*now + 1))
;;

let rec mod_op l r x now = match now with
  |_ when  tree.(now).maxx < x -> ();
  |_ when tree.(now).l = tree.(now).r ->
    ( tree.(now).sum <- Num.mod_num tree.(now).sum (Num.num_of_int x);
       tree.(now).maxx <- Num.int_of_num tree.(now).sum )
  |_ ->(
      (
      match l with
      |_ when l > tree.(now).mid -> mod_op l r x (2*now+1)
      |_ when r <= tree.(now).mid -> mod_op l r x (2*now)
      |_ ->( mod_op l tree.(now).mid x (2*now);
             mod_op (tree.(now).mid + 1) r x (2*now + 1) )
      );
      tree.(now).sum <- Num.add_num tree.(2*now).sum
          tree.(2*now+1).sum;
      tree.(now).maxx <- max tree.(2*now).maxx tree.(2*now+1).maxx
    )
;;

let rec set_op k x now = match now with
  |_ when tree.(now).l = k && tree.(now).r = k ->
    tree.(now).maxx <- x;
    tree.(now).sum <- Num.num_of_int x
  |_ ->( 
      (match k with
       |_ when k > tree.(now).mid  -> set_op k x (2*now+1)
       |_ -> set_op k x (2*now)
      );
      tree.(now).sum <- Num.add_num tree.(2*now).sum tree.(2*now+1).sum;
      tree.(now).maxx <- max tree.(2*now).maxx tree.(2*now+1).maxx
    )
;;

let print_info () =
  printf  "%s " "array: "; 
  for i = 1 to n do
    printf "%s  "  (Num.string_of_num (query i i 1) )
  done;
  print_endline ""
;;

let print_info () = ()
;;      

for i = 0 to m - 1 do 
  let x = Scanf.bscanf Scanf.Scanning.stdin "%d " ( fun x -> x) in
  (*print_int x; print_endline "";*)
  match x with
  | 1  -> let (l,r) = Scanf.bscanf Scanf.Scanning.stdin "%d %d " (
      fun x y -> (x,y) ) in let rslt = query l r 1 in
    print_endline (Num.string_of_num rslt);
    print_info ()
  | 2 -> let (l,r,x) = Scanf.bscanf Scanf.Scanning.stdin "%d %d %d " (fun x
                                                                       y z -> (x,y,z)) in
    mod_op l r x 1; print_info ()
  | 3 -> let (k,x) = Scanf.bscanf Scanf.Scanning.stdin "%d %d " (
      fun x y -> (x,y) ) in
    set_op k x 1; print_info ()
done 
;;
