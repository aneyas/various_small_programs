let s =  Scanf.scanf "%s " ( fun x -> x) |> String.lowercase

let h = "hello";;
let j = ref 0;;
let c = ref (String.get h !j);;
let flag = ref "NO";;

exception Success;;
  try
for i = 0 to String.length s - 1 do
  let si = String.get s i in
  if(si == !c) then
    begin
      incr j;
      if (!j > 4) then raise Success;
      c := String.get h !j;
    end
done
  with Success -> flag := "YES";;

    print_endline !flag;;
