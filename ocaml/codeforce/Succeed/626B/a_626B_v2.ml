open Scanf;;
open String;;
open Printf;;
open Int64;;

(* This version implements 
 * a(n) = a(n-1) + a(n-k)
 * where k is the number of continued W
 * - Reduce memory usage.
 *)

(*
let (t,k) = bscanf Scanning.stdin "%d %d " (fun x y -> (x,y) )
*)
let k = bscanf Scanning.stdin "%d " (fun x -> x )
let s = bscanf Scanning.stdin "%s " (fun x -> x )

let nc = Array.make 3 0

for i = 0 to k-1 do
  let x = get s i in 
  match x with
  | 'B' -> nc.(0)<-nc.(0)+1
  | 'G' -> nc.(1)<-nc.(1)+1
  | 'R' -> nc.(2)<-nc.(2)+1
done
;;

type ou = 
  |O |B |G |R |BG |BR |GR | BGR

  (* for debugging*)
let c2i = function
  |O -> 0
  |B -> 1
  |G -> 2
  |R -> 3
  |BG -> 4
  |BR -> 5
  |GR -> 6
  |BGR ->7
;;

let pc = function
  |O -> print_endline "O"
  |B -> print_endline "B"
  |G -> print_endline "G"
  |R -> print_endline "R"
  |BG -> print_endline "BG"
  |BR -> print_endline "BR"
  |GR -> print_endline "GR"
  |BGR ->print_endline "BGR"
;;

let i2c = function
  |0 -> O
  |1 -> B
  |2 -> G
  |3 -> R
  |4 -> BG
  |5 -> BR
  |6 -> GR
  |7 -> BGR
;;

(* union color *)
let rec unc a b = 
  if a = b then
    a
  else
    begin
      match (a,b) with
      | (h, t) when t = O -> h
  | (h, t) when h = O -> t
  | (c, O) -> c
  | (BGR,_) -> BGR
  | (_,BGR) -> BGR
  | (B,G) -> BG
  | (B,R) -> BR
  | (B,BG) -> BG
  | (B,BR) -> BR
  | (B,GR) -> BGR
  | (G,R) -> GR
  | (G,BG) -> BG
  | (G,BR) -> BGR
  | (G,GR) -> GR
  | (R,BG) -> BGR
  | (R,BR) -> BR
  | (R,GR) -> GR
  | (BG,BR) -> BGR
  | (BG,GR) -> BGR
  | (BR,GR) -> BGR
  | (_,_) -> unc b a
    end

    (*
let a = BG in
let b = O in
let c = unc a b in
print_int (c2i c)
*)
let nn = 201

let a = Array.init nn (fun _ -> Array.init nn (fun _ -> (Array.make nn (-1))))

let rec f k l m = (*in order of b, g, r*)
  if k<0 || l<0 || m<0 then
    O
  else if a.(k).(l).(m) >= 0 then (* a matrix records if k,l,m element calculated*)
    i2c a.(k).(l).(m)   (* b matrix contains the calculated result *)
  else
    match (k,l,m) with
    | (1,0,0) -> B
    | (0,1,0) -> G
    | (0,0,1) -> R
    | (_,_,_) ->
        begin
          let a1 = if k>=2 then f (k-1) l m else O in
          let a2 = if l>=2 then f k (l-1) m else O in
          let a3 = if m>=2 then f k l (m-1) else O in
          let a4 = f (k-1) (l-1) (m+1) in
          let a5 = f (k-1) (l+1) (m-1) in
          let a6 = f (k+1) (l-1) (m-1) in
          let x = List.fold_right unc [a1;a2;a3;a4;a5;a6] O in
          a.(k).(l).(m) <- (c2i x);  x
    end
;;

(*tests*)
(*
let c = f 1 0 1 in
pc c in
let c = f 0 2 1 in
pc c in
*)
let c = f nc.(0) nc.(1) nc.(2) in
pc c

