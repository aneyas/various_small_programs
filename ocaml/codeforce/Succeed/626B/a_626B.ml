open Scanf;;
open String;;
open Printf;;
open Int64;;
open Bigarray;;

(* This version implements 
 * a(n) = a(n-1) + a(n-k)
 * where k is the number of continued W
 * - Reduce memory usage.
 *)

let (t,k) = bscanf Scanning.stdin "%d %d " (fun x y -> (x,y) )

type ou = 
  |O |B |G |R |BG |BR |GR | BGR

  (* for debugging*)
let c2i = function
  |O -> 0
  |B -> 1
  |G -> 2
  |R -> 3
  |BG -> 4
  |BR -> 5
  |GR -> 6
  |BGR ->7
;;

let i2c = function
  |0 -> O
  |1 -> B
  |2 -> G
  |3 -> R
  |4 -> BG
  |5 -> BR
  |6 -> GR
  |7 -> BGR

(* union color *)
let rec unc a b = 
  if a = b then
    a
  else
    begin
      match (a,b) with
  | (h, t) when t = O -> h
  | (h, t) when h = O -> t
  | (c, O) -> c
  | (BGR,_) -> BGR
  | (_,BGR) -> BGR
  | (B,G) -> BG
  | (B,R) -> BR
  | (B,BG) -> BG
  | (B,BR) -> BR
  | (B,GR) -> BGR
  | (G,R) -> GR
  | (G,BG) -> BG
  | (G,BR) -> BGR
  | (G,GR) -> GR
  | (R,BG) -> BGR
  | (R,BR) -> BR
  | (R,GR) -> GR
  | (BG,BR) -> BGR
  | (BG,GR) -> BGR
  | (BR,GR) -> BGR
  | (_,_) -> unc b a
    end

 (*
let a = BG in
let b = O in
let c = unc a b in
print_int (c2i c)
*)
let nn = 40

let a = Array3.create int c_layout nn nn nn

let () = Array3.fill a (-1)

let rec f k l m = (*in order of b, g, r*)
  if a.{k,l,m} >= 0 then (* a matrix records if k,l,m element calculated*)
    i2c a.{k,l,m}   (* b matrix contains the calculated result *)
  else
    match (k,l,m) with
    | (1,0,0) -> B
    | (0,1,0) -> G
    | (0,0,1) -> R
    | (b,g,r) when b<0 || g < 0 || r < 0 -> O
    | (_,_,_) ->
    begin
      let a1 = f (k-1) l m in
      let a2 = f k (l-1) m in
      let a3 = f k l (m-1) in
      let a4 = f (k-1) (l-1) (m+1) in
      let a5 = f (k-1) (l+1) (m-1) in
      let a6 = f (k+1) (l-1) (m-1) in
      let x = List.fold_right unc [a1;a2;a3;a4;a5;a6] O in
      a.{k,l,m} <- (c2i x);  x
    end


