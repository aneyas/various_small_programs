(**D. The Child and Sequence*)

open Scanf;;
open Num;;
open Printf;;

let p,n = Scanf.bscanf Scanf.Scanning.stdin "%d %d " ( fun x y -> x,y );;

let hashv = Array.create p 0;;

exception BreakLoop;;

let j = ref 0;;
let rslt = ref (-1);;
try
  for i = 1 to n do
    j := !j + 1;
    let x = Scanf.bscanf Scanf.Scanning.stdin "%d " ( fun x -> x) in
    let y = x mod p in
    match hashv.(y) with
    | 0 -> hashv.(y) <- 1
    | 1 -> raise BreakLoop 
  done;
with  BreakLoop -> rslt := !j;;

printf "%d\n" !rslt;;

