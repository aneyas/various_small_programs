open Scanf
open Num
open Printf

let n = Scanf.bscanf Scanf.Scanning.stdin "%d " ( fun x -> x)

let () = 
  if n <= 2 then print_endline "NO"
  else 
    begin
      let m  = (n mod 2) in
      match m with
  | 0 -> print_endline "YES"
  | _ -> print_endline "NO"
    end
