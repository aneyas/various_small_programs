open Scanf;;
open String;;
open Printf;;
open Num;;

let n = bscanf Scanning.stdin "%d " (fun x -> x);;

let nn = 100000 + 5;;
let count = Array.make nn (num_of_int 0);;
let indx = Array.make nn 0;;
let score = Array.make nn (num_of_int 0);;

let p = Array.init n (fun x -> 0);;

for i=0 to n-1 do
  let  f = bscanf Scanning.stdin "%d " (fun x -> x)  in
  count.(f) <- count.(f) +/ (num_of_int f)
done;;

let l = ref 1;;

for i=1 to nn-1 do
  if count.(i) >/ num_of_int 0 then
    begin
      indx.(!l) <- i;
      l := !l + 1
    end
done;;

(*For testing
for i=1 to !l-1 do
  printf "%d %d\n" indx.(i) count.(indx.(i));
done;;*)

score.(0) <- num_of_int 0;;
score.(1) <- count.(indx.(1));;
let cur_indx = ref indx.(1) in
for i=2 to !l-1 do
  let x1 = !cur_indx in
  let x2 = indx.(i) in
  if x2 > x1 + 1 then
    begin
      score.(i)<-score.(i-1) +/  count.(x2);
      cur_indx := indx.(i)
    end
  else
    begin
      let tscore = score.(i-2) +/ count.(x2) in
      if tscore >/ score.(i-1) then
        begin
          score.(i) <- tscore;
          cur_indx := x2
        end
      else
        begin
          score.(i) <- score.(i-1);
          cur_indx := indx.(i-1)
        end
    end
done;;

print_endline (string_of_num (score.(!l-1)));; 

