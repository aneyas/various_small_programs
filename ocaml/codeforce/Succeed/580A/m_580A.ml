open Printf
open String

(*let n = Scanf.bscanf Scanf.Scanning.stdin "%d " ( fun x -> x) ;;
 *)
  let n = Scanf.scanf "%d " (fun x -> x);;

let a = Array.make n (-1);;

for i = 0 to n-1 do
  let jj = Scanf.bscanf Scanf.Scanning.stdin "%d " (fun x -> x) in
  a.(i) <- jj
done

let c = Array.make n  1;; (* This is the array counting the number of increasing sequence *)

let d = ref 1;;

  for i = 1 to n-1 do
    begin
      if( a.(i) >= a.(i-1) ) then
        c.(i) <- c.(i-1) + 1;
      if( !d < c.(i) ) then d := c.(i)
    end
  done;;

let () = print_int !d; print_endline "";;
