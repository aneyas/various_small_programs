open Scanf
open Num
open Printf
open String

let n = Scanf.bscanf Scanf.Scanning.stdin "%d " ( fun x -> x)

for i = 1 to n do
  let s = Scanf.bscanf Scanf.Scanning.stdin "%s " ( fun x -> x) in
  let l = length s in 
  if ( l <= 10) then
    print_endline s
  else
      let s1 = String.make 1 (String.get s 0) in
      let se = String.make 1 (String.get s ( l - 1)) in
      let num = string_of_int ( l - 2 ) in 
      print_endline (s1 ^ num ^ se)
done
