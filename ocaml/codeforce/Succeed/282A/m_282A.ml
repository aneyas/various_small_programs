let read_int () = Scanf.scanf "%d " (fun x -> x);;
let read_int3 () = Scanf.scanf " %d %d %d " (fun x y z -> (x,y,z) );;

let inc_or_dec = function
  | "X++" -> 1
  | "++X" -> 1
  | _ -> -1
;;


let read_s () = Scanf.scanf "%s " (fun x -> inc_or_dec x );;

let n = read_int ();;

let a = Array.init n (fun i -> (read_s () ));;


let b = Array.fold_left (+) 0 a in print_int b; print_endline ""
