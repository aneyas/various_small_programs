#include <stdio.h>

const int N=100005;

int a[N];
int b[N];
int c[N];
int d[N];

int main(){
  int n,m;
  scanf("%d %d ",&n,&m);
  for(int i=0; i< n; i++){
    scanf("%d ", &a[i]);
  }

  for(int i=0;i<N;i++) {
    b[i]=0;
    c[i]=0;
  }

  for(int i=0; i< m; i++){
    int tmp;
    scanf("%d ", &tmp);
    c[tmp] = 1;
    d[i] = tmp;
    //printf("%d\n",tmp);
  }
  //printf("====\n");

  int count = 0;
  for(int j = n-1; j>=0; j--){
    int tmp = a[j];
    if(!b[tmp]){
      count++;
      b[tmp] = 1;
    }
    if(c[j+1]) {
    //  printf("%d\n",j);
      c[j+1] = count;
    }
  }

  for(int i = 0; i< m; i++){
    printf("%d \n", c[d[i]]);
  }

  return 0;
}
