open Scanf
open Printf

let n = Scanf.scanf "%d " ( fun x -> x )

let nn = n + 10

let a = Array.make nn (-1);;
let b = Array.make nn (-1);;
let c = Array.make nn (-1);; (*The number of people on tram after each stop*)

for i = 1 to n do
  let (ai,bi) = Scanf.bscanf Scanf.Scanning.stdin "%d %d " ( fun x y -> (x,y) ) in
  a.(i) <- ai; b.(i) <- bi
done
;;

let () = c.(0) <- 0;;

for i = 1 to n do
  c.(i) <- (c.(i-1) + b.(i-1) - a.(i-1))
done
;;

let z = Array.fold_left (fun x y -> if x > y then x else y ) (-1) c
in print_int z; print_endline ""
