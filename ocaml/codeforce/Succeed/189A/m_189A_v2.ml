open Printf

let (n,a,b,c) = Scanf.scanf "%d %d %d %d " (fun x a b c -> (x,a,b,c));;

let min_cut = min a (min b c);;
let d = Array.make (n+1) 0;;
let () = d.(0) <- 1;;

  for i = 0 to n do
    if (d.(i)>0) then
      begin
        if (i+a<=n && d.(i)+1 > d.(i+a) ) then d.(i+a) <- d.(i)+1;
        if (i+b<=n && d.(i)+1 > d.(i+b) ) then d.(i+b) <- d.(i)+1;
        if (i+c<=n && d.(i)+1 > d.(i+c) ) then d.(i+c) <- d.(i)+1;
      end
    (* Array.iter (fun x -> printf "%d " x) d;
      printf "\n"
     *)
  done;;

let () = printf "%d\n" (d.(n)-1);;
