open Printf

let (n,a,b,c) = Scanf.scanf "%d %d %d %d " (fun x a b c -> (x,a,b,c));;

let min_cut = min a (min b c);;
let d = Array.make (n+1) 0;;

let flag = Array.make (n+1) false in
    flag.(0)<- true;

    for i = 0 to n do
      if (flag.(i)) then
        begin
          if (i+a<=n && d.(i)+1 > d.(i+a) ) then
            begin
              d.(i+a) <- d.(i)+1;
              flag.(i+a)<-true
            end;
          if (i+b<=n && d.(i)+1 > d.(i+b) ) then
            begin
              d.(i+b) <- d.(i)+1;
              flag.(i+b)<- true
            end;
          if (i+c<=n && d.(i)+1 > d.(i+c) ) then
            begin
              d.(i+c) <- d.(i)+1;
              flag.(i+c) <- true
            end;
        end;
     (* Array.iter (fun x -> printf "%d " x) d;
      printf "\n"
      *)
  done

let () = printf "%d\n" d.(n);;
