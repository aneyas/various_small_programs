let read_int () = Scanf.scanf "%s " (fun x -> x);;

let explode s =
  let r = ref [] in
  for i = 0 to (String.length s - 1) do
    let c = String.get s i in
    r := !r@[c]
  done; !r
;;
let s = read_int () |> explode |> List.filter (fun x -> (x <> '+'))
        |> List.sort (fun x y -> Char.compare x y);;

(* List.iter (fun x -> print_char x) s;;*)
  let rec printx l =
    match l with
    | [a] -> print_char a; print_endline ""
    | a :: [b] -> print_char a; print_char '+'; print_char b; print_endline ""
    | a :: rest -> print_char a; print_char '+'; printx rest
      in printx s;;

  (*
  let r = Str.regexp "hello \\([A-Za-z]+\\)" in
        Str.replace_first r "\\1" "hello world"
        *)
