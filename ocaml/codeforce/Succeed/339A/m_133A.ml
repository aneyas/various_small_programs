let flag = ref "NO";;

exception Found;;

let vote = function
  | 'H' -> raise Found
  | 'Q' -> raise Found
  | '9' -> raise Found
  | _ -> ()
;;

let s = Scanf.scanf "%s " (fun x ->  x)

try
  for i=0 to String.length s - 1 do
    vote (String.get s i)
  done
with Found -> flag := "YES";;

print_endline !flag;;
