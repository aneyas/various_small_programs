open Scanf
open Num
open Printf
open String

let (n, k) = Scanf.bscanf Scanf.Scanning.stdin "%d %d " ( fun x y -> (x,y))


let scores  = 
  let rec input_score i ll =
    match i with
  | 0 -> ll
  | _ -> begin
    let x = Scanf.bscanf Scanf.Scanning.stdin "%d " ( fun x -> x) in
    input_score (i-1) ll@[x]
  end
  in List.rev (input_score n [])
;;

  (*DEBUG
List.iter (fun x -> print_int x; print_endline " ") scores;;
List.iter (fun x -> print_int x; print_endline " ") purged_scores;;
*)

let purged_scores = List.filter (fun x -> (x > 0)) scores;;
let num_valid_scores = List.length purged_scores;;

let remain n ll =
  let rec helper k ll =
    match k with
    | 0 -> ll
    | _ -> helper (k-1) (List.tl ll)
  in helper n ll
;;

if(num_valid_scores <= k) then
  begin
    print_int num_valid_scores; print_endline ""
  end
else
  begin
    let kscore = List.nth purged_scores (k-1) in
    let rslt = List.filter (fun x -> (x==kscore)) (remain k purged_scores) in
    print_int (k + List.length rslt); print_endline ""
  end

