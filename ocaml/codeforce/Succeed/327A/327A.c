#include<stdio.h>


int
count(int * x, int a, int b){
  int rslt = 0;
  for(int i = a; i<=b; i++){
    rslt += x[i];
  }
  return rslt;
}

int
main(){
  const int N = 110;
  int n;
  int a[N];
  int d[N][N];

  scanf("%d",&n);
  for(int i = 0; i< n; i++){
    scanf("%d",&a[i]);
  }

  int num_of_1 = count(a,0,n-1);
 
  int max_1 = -1;
  for(int i = 0; i < n; i++){
    d[i][i] =  num_of_1 - a[i] + !a[i];
    max_1 = max_1<d[i][i]? d[i][i]:max_1;
  }

//  //DEBUG
//  printf("--%d\n",num_of_1);
//  for(int i = 0; i < n; i++){
//    printf("==%d\n",d[i][i]);
//  }

  for(int i = 0; i < n; i++){
    for(int j = i+1; j < n; j++){
      d[i][j] = d[i][j-1] - a[j] + !a[j];
      max_1 = max_1<d[i][j]? d[i][j]:max_1;
    }
  }

  printf("%d\n", max_1);


  return 0;
}

