open Scanf
open Num
open Printf
open String

let (w,h) = Scanf.bscanf Scanf.Scanning.stdin "%d %d " ( fun x y -> x,y) ;;

let area = w * h;;
let ntotal = ref 0;;

let () =
  if (area < 2) then
    ntotal := 0
  else
    begin
      if (w >= 2) then
        let nw = w / 2 in
        ntotal := !ntotal + (nw* h);
        ntotal := !ntotal + ((w mod 2)*(h/2))
      else
        ntotal := !ntotal + (h/2)
    end
;;

let () = print_int !ntotal; print_endline "";;
