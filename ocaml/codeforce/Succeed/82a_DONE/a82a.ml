(**D. The Child and Sequence*)

open Scanf;;
open Num;;
open Printf;;

let n = Scanf.bscanf Scanf.Scanning.stdin "%d " ( fun x -> x);;

let person = [|"Sheldon";"Leonard";"Penny";"Rajesh";"Howard"|];;

(*
let rec find_m  nn m twom = 
    let drinked_cola = 5 * (twom - 1) in
    if drinked_cola < nn then
        find_m nn (m + 1) (2 * twom)
    else if drinked_cola = nn then
        ( m, 0)
    else
        (m, nn - 5*(twom/2 - 1))
;;

let (m, remained) = find_m n 1 2;;
*)

let rec pow a = function
  | 0 -> 1
  | 1 -> a
  | n -> 
    let b = pow a (n / 2) in
    b * b * (if n mod 2 = 0 then 1 else a)
;;

let mm = int_of_float (log ( (float_of_int n) /. 5. +. 1.) /. log ( 2.0) );;
let remained = n - 5 * ( (pow 2 mm) - 1);;

let m = mm + 1;;
(*printf "%d | %d %d \n\n" n m remained;;*)

if m = 0 then
    raise (Failure "ERROR")
else if m = 1 then
    if remained = 0 then
        raise (Failure "ERROR")
    else
        print_endline person.(remained - 1)
else
    if remained = 0 then
        print_endline person.(4)
    else
        print_endline person.(remained / (pow 2  (m - 1 ) ))
;;
