#include<stdio.h>
#include<string.h>
#include<stdbool.h>

void
main(){
  const int N = 100009;
  char a[100009];
  scanf("%s",a);
  int len = strlen(a);

  bool d[N][8];
  //  bool flag1_A = false; 0
  //  bool flag1_B = false; 1
  //
  //  bool flag2_A = false; 2
  //  bool flag2_B = false; 3
  //
  //  bool flag3_A = false; 4
  //  bool flag3_B = false; 5
  //
  //  bool flag4_A = false; 6
  //  bool flag4_B = false; 7

  //Debug
  //printf("%d\n",len);

  for(int i = 0; i < N; i++){
    for(int j = 0; j < 8; j++){
      d[i][j] = false;
    }
  }

  d[0][0] = (a[0]=='A');
  d[0][1] = (a[0]=='B');

  for(int i = 1; i < len; i++){
    if(a[i]=='A'){
      d[i][2] = d[i-1][1] || d[i-1][2];
      d[i][4] = d[i-1][2];
      d[i][6] = d[i-1][5];

      if(d[i][6]){
        printf("YES\n");
        return;
      }
      d[i][0] = true;
      d[i][3] = d[i-1][3];
    }else if(a[i]=='B'){
      d[i][3] = d[i-1][0] || d[i-1][3];
      d[i][5] = d[i-1][3];
      d[i][7] = d[i-1][4];

      if(d[i][7]){
        printf("YES\n");
        return;
      }

      d[i][1] = true;
      d[i][2] = d[i-1][2];

    }else{
      d[i][2] = d[i-1][2];
      d[i][3] = d[i-1][3];
    }

  }

  printf("NO\n");
}
