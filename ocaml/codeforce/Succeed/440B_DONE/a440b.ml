open Scanf;;
open Num;;
open Printf;;

let n = Scanf.bscanf Scanf.Scanning.stdin "%d " ( fun x -> x);;

let imatch = Array.create n (num_of_int 0);;
let kmatch = ref (Num.num_of_int 0);;

for i = 0 to n-1 do
  let ni = Scanf.bscanf Scanf.Scanning.stdin "%d " ( fun x -> x) in
  imatch.(i) <- num_of_int ni; kmatch :=  (num_of_int ni) +/ !kmatch
done
;;

(*Remove the average*)
let av = div_num !kmatch (num_of_int n) in
for i = 0 to n - 1 do
  imatch.(i) <- imatch.(i) -/ av
done
;;

(*Array.iter (fun x -> print_int x; print_endline "") imatch;;*)

let sum_over i j =
  let total = ref (num_of_int 0) in
  for k = i to j do
    total := !total +/  imatch.(k)
  done; total
;;

let rec direct_cal nl nr rslt = 
  if nl = nr then
    rslt +/ !(sum_over nl nr)
  else
    let mid = (nl + nr) / 2 in
    let a = !(sum_over nl mid) in
    imatch.(mid) <- imatch.(mid) -/  a;
    imatch.(mid+1)<-imatch.(mid+1) +/  a;
    rslt +/ (abs_num a) +/ (direct_cal nl mid (num_of_int 0)) +/
      (direct_cal (mid + 1) nr (num_of_int 0)) 
;;

let rslt = direct_cal 0 (n-1) (num_of_int 0) in
print_endline (string_of_num 
                 (abs_num rslt) );;

(* earlier dubug
printf "%d %d\n" n !kmatch
*)
