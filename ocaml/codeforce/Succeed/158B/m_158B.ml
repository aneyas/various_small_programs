let read_int () = Scanf.scanf "%d " (fun x -> x);;
let read_int3 () = Scanf.scanf " %d %d %d " (fun x y z -> (x,y,z) );;

let n = read_int ();;

let a = Array.init n (fun i -> read_int () );;

let () = a |> Array.fast_sort (fun x y -> (if (x < y ) then 1 else -1) );;


let cars = Array.make n 0;; (* Call n empty cars *)


exception BreakLoop;;
let start = ref 0;;

  for i = 0 to n-1 do
    try
    for j = !start to n-1 do
      let x = a.(i) + cars.(j) in
      if ( x <= 4 ) then
        begin
          cars.(j) <- x;
          raise BreakLoop
        end
    done
    with BreakLoop -> ()

  done;;

  (*    Array.iter (fun x -> print_int x; print_endline "") cars;;*)

  let num_cars = ref 0;;
    try
    for i = 0; to n-1 do
      if(cars.(i)>0) then
        num_cars := !num_cars + 1
      else
        raise BreakLoop
    done
    with BreakLoop -> ()
    ;;

let () = (print_int !num_cars; print_endline "");;

         (* This is the easy way *)
(*
         let b = Array.to_list cars
        |> List.filter (fun x -> (x>0))
        |> List.length
;;

let ()= print_int b; print_endline "";;
 *)
