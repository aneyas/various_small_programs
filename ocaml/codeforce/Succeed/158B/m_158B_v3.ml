let read_int () = Scanf.scanf "%d " (fun x -> x);;
let read_int3 () = Scanf.scanf " %d %d %d " (fun x y z -> (x,y,z) );;

let numx = Array.make 4 0;;
let n = read_int ();;
  for i = 1 to n do
    Scanf.scanf "%d " (fun x -> numx.(x-1)<- numx.(x-1)+1)
  done

let num_cars = ref ( numx.(3) + numx.(1)/2 );;
let () = numx.(1) <- numx.(1) mod 2;;
let () = if (numx.(0) <= numx.(2)  ) then
           begin
             numx.(2) <- numx.(2) - numx.(0);
             num_cars := !num_cars + numx.(0);
             numx.(0) <- 0
           end
         else
           begin
             num_cars := !num_cars + numx.(2);
             numx.(0) <- numx.(0) - numx.(2);
           end
;;

let () = if (numx.(0) == 0) then
           num_cars := !num_cars + numx.(2) + numx.(1)
         else
           begin
             if(numx.(1) == 1) then
               begin
                 num_cars := !num_cars + 1;
                 numx.(0) <- numx.(0) -2
               end;
             if (numx.(0) > 0) then
               num_cars := !num_cars + ( numx.(0) + 3) /4
           end
;;
let ()= print_int !num_cars; print_endline "";;
