#include <stdio.h>

const int N=400;

int d[N][N];

int
f (int m,int n) {

  if(d[m][n]>=0) { d[n][m] = d[m][n]; return d[m][n];}
  if(d[n][m]>=0) { d[m][n] = d[n][m]; return d[n][m];}

  //printf("%d %d\n", m,n);

  if(m<=0 || n<=0) {d[m][n] = 0; return 0;}
  if(m==1 && n==1) {d[m][n] = 0; return 0;}
  if(m==1) {d[m][n] = f(2,n-2) + 1; return d[m][n];}
  if(n==1) {d[m][n] = f(m-2,2) + 1; return d[m][n];}

  int a = f(m+1,n-2) + 1;
  int b = f(m-2,n+1) + 1;

  if (a>b) {d[m][n] = a; return a;};
  d[m][n] = b; return b;
}

int main(){
  int a,b;
  scanf("%d %d ",&a,&b);

  for(int i = 0; i< N; i++){
    for(int j = 0; j< N; j++){
      d[i][j] = -1;
    }
  }


  int rslt = f(a,b);
  printf("%d\n", rslt);
  return 0;
}
