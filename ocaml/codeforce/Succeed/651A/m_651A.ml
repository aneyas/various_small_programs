open Printf

let (aa,bb) = Scanf.scanf "%d %d " (fun a b  -> (a,b));;

let nn = 400

let d = Array.init nn (fun i -> Array.make nn (-1))

(*Initialize the boundary conditions*)
for i = 0 to nn do
  d.(0).(i) <- 0;
  d.(i).(0) <- 0;
done;;

let rec f m n = 
  begin
    printf "%d %d\n" m n;
  match (m,n) with
  | (-1, _) -> 0
  | (_, -1) -> 0
  | (0, _) -> 0
  | (_, 0) -> 0
  | (1, 1) -> 0
  | (1, a) -> (f 2 (a-2))+1
  | (a, 1) -> (f (a-2) 2)+1
  | (a,b) -> (max (f (m+1) (n-2)) (f (m-2) (n+1))) + 1
  end
;;

let a = f 2 0

(*let a = f 2 1 in
print_int a; print_endline "";;
*)
