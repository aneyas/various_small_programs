let read_int () = Scanf.scanf "%s " (fun x -> x);;

let s = read_int ();;
let flag = ref (String.get s 1);;
let c = ref 1;;

  let rslt = ref "NO";;

exception Found;;

  try
  for i = 1 to String.length s - 1 do
    let x = String.get s i in
    if (x == !flag) then
      begin
        c := !c +1;
        if(!c>=7) then raise Found
      end
    else
      begin
        c := 1;
        flag := x
      end
  done
  with Found -> rslt := "YES"
  ;;

  let () = print_endline !rslt;;
