let read_int () = Scanf.scanf "%s " (fun x -> x);;

let s1 = read_int () |> String.lowercase;;
let s2 = read_int () |> String.lowercase;;

let x = String.compare s1 s2 in
    print_int x; print_endline "";;
