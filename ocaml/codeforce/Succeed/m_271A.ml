open Printf

exception Found

let n =  Scanf.scanf "%d " ( fun x -> x)

let y = ref (n+1)

let not_distinct year =
  let flag = ref false in
  try
  let s = string_of_int year in
  let l = String.length s in
  for i = 0 to l-1 do
    for j =i+1 to l -1 do
      let x = String.get s i in
      let y1 = String.get s j in
      if(x==y1) then begin flag := true; raise Found end
    done;
  done; 
  !flag;
  with Found -> !flag
;;

let () =
  while( (not_distinct !y) ) do
    incr y
  done
;;

printf "%d\n" !y
