open Scanf;;
open String;;
open Printf;;
open Int64;;

let k = bscanf Scanning.stdin "%d " (fun x -> x )
let s = bscanf Scanning.stdin "%s " (fun x -> x )

let to_list s =
  let l = ref [] in
  for i = 0 to (length s) - 1 do
    let x = get s i in
    l := x :: !l
  done; l
;;

let ls = !(to_list s)

(*
let () = List.iter (fun x -> print_char x; print_endline "") ls
*)

let rec cc l = 
  match l with 
  |[] -> 0
  |x::[]->0
  |x::y::tl when x=y -> 1 + (cc (x::tl))
  |x::tl -> cc tl

let n = cc ls in
print_int n; print_endline ""
;;



