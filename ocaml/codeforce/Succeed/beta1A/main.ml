open Scanf;;
open Num;;
open Printf;;
open Int64;;

let (n,m,a) = Scanf.bscanf Scanf.Scanning.stdin "%d %d %d " (fun x y z -> (x, y, z));;

(*
print_int n; print_endline "";;
print_int m; print_endline "";;
print_int a; print_endline "";;
 *)

let mprint z =
print_endline (Num.string_of_num z);;
  
let rslt  =
  let x = Num.num_of_int(n / a) in
  let y = Num.num_of_int(m / a) in
  let aa = Num.num_of_int(a) in
  let nn = Num.num_of_int(n) in
  let mm = Num.num_of_int(m) in
  let one = num_of_int(1) in
  match (x*/aa </ nn, y*/aa </ mm) with
  | (true,true) -> (x+/one)*/(y+/one)
  | (true,false) -> (x+/one)*/y
  | (false,true) ->  x*/(y+/one)
  | (false,false) ->  x*/y
    in mprint rslt  
