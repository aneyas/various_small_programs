open Scanf;;
open String;;


let n = bscanf Scanning.stdin "%d " (fun x -> x);;
let m = bscanf Scanning.stdin "%d " (fun x -> x);;

let p = Array.init m (fun x -> 0);;

for i=0 to m-1 do
  let  f = bscanf Scanning.stdin "%d " (fun x -> x)   in
  p.(i) <- f
done;;

(*Sort the array ascendingly*)
Array.sort (fun x y ->  x-y ) p;;

(*Test the sorting result
Array.iter (fun x -> Printf.printf "%d " x)  p;;
*)

let i = ref 0 in
let maxdiff = ref  max_int in
let () = 
  while (!i + n-1) <= (m-1) do
    let diff =  p.(!i+n-1) -p.(!i) in
    if ( diff < !maxdiff) then  maxdiff := diff ;
    i := !i + 1
  done in
print_int !maxdiff; print_endline ""
;;
