open Scanf;;
open String;;
open Printf;;
open Int64;;

(* This version implements 
 * a(n) = a(n-1) + a(n-k)
 * where k is the number of continued W
 * - Reduce memory usage.
 *)

let (t,k) = bscanf Scanning.stdin "%d %d " (fun x y -> (x,y) )

let modn i = rem i (of_int 1000000007)

let ni i = of_int i
let sn i = to_string i

(*let nn = 100000 + 5*)
let nn_max = 100000 + 5

let nn = 100000 + 3

let r= Array.make nn_max (ni 0)

for i = 1 to k-1 do
  r.(i) <- (ni 1)
done

let () = 
 r.(k) <- ni 2

for i = k+1 to nn do
  r.(i) <-  modn (add r.(i-1) r.(i-k))
done

for i = 2 to nn do
    r.(i) <- add r.(i) r.(i-1)
done

for i = 1 to t do
   let (s,e) = bscanf Scanning.stdin "%d %d " (fun x y -> (x,y) ) in
   let sum = ref (ni 0) in
   sum := sub r.(e) r.(s-1);
   printf "%s\n" (sn (modn !sum) )
done


