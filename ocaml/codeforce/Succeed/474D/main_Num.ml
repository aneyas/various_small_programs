open Scanf;;
open String;;
open Printf;;
open Num;;

let (t,k) = bscanf Scanning.stdin "%d %d " (fun x y -> (x,y) )

let modn i = mod_num i (num_of_int 1000000007)

let ni i = num_of_int i
let sn i = string_of_num i

(*let nn = 100000 + 5*)
let nn_max = 100000 + 5

let nn = 100000 + 3

let r= Array.make nn_max (ni 0)
let w= Array.make nn_max (ni 0)

let num_w = ref (ni 1)

let () = 
 r.(1) <- ni 1

if k = 1 then
begin
  w.(1) <- ni 1; 
end
else
begin
  w.(1) <- ni 0;
end

for i = 2 to nn do
   r.(i) <- r.(i-1) +/ w.(i-1);
   w.(i) <- w.(i-1);
   num_w := !num_w +/ (ni 1);

   if !num_w = (ni k) then
   begin
   w.(i) <- w.(i) +/ (ni 1);
num_w := ni 0;
end
done

for i = 95515 to nn do
    printf "%s %s\n" (sn r.(i)) (sn w.(i))
done

for i = 1 to t do
   let (s,e) = bscanf Scanning.stdin "%d %d " (fun x y -> (x,y) ) in
   let sum = ref (ni 0) in
   for j = s to e do
       sum := !sum +/ r.(j) +/ w.(j)
   done;
   printf "%s\n" (sn !sum )
done


