open Scanf;;
open String;;
open Printf;;
open Num;;

(* This version implements 
 * a(n) = a(n-1) + a(n-k)
 * where k is the number of continued W
 *)

let (t,k) = bscanf Scanning.stdin "%d %d " (fun x y -> (x,y) )

let modn i = mod_num i (num_of_int 1000000007)

let ni i = num_of_int i
let sn i = string_of_num i

(*let nn = 100000 + 5*)
let nn_max = 100000 + 5

let nn = 100000 + 3

let r= Array.make nn_max (ni (-1))

for i = 1 to k-1 do
  r.(i) <- (ni 1)
done

let () = 
 r.(k) <- ni 2

let f n = 
  if r.(n) = (ni (-1)) then
    begin
    let x = r.(n-1) +/ r.(n-k) in 
    r.(n) <- x; x
    end
  else
    r.(n)

for i = k+1 to nn do
  r.(i) <- (f (i-1)) +/ (f (i-k))
done

(*
for i = 95515 to nn do
    printf "%s %s\n" (sn r.(i)) (sn w.(i))
done
*)

for i = 1 to t do
   let (s,e) = bscanf Scanning.stdin "%d %d " (fun x y -> (x,y) ) in
   let sum = ref (ni 0) in
   for j = s to e do
       sum := !sum +/ r.(j)
   done;
   printf "%s\n" (sn (modn !sum) )
done


