open Printf
type room = R of int*int

let n =  Scanf.scanf "%d " ( fun x -> x) in
let read_int () =  Scanf.scanf "%d %d " ( fun x y -> R(x,y)) in
let a = Array.init n (fun i -> read_int ()) in
let f = function
  | R (p,q) -> if q-p >=2 then true else false
in Array.to_list a |>List.filter (fun x -> f x) |> List.length |> printf "%d\n"
