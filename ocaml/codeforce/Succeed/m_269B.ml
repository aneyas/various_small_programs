open Scanf
open Num
open Printf
open String

let s = Scanf.bscanf Scanf.Scanning.stdin "%s " ( fun x -> x) |> String.lowercase

let explode s =
  let r = ref [] in
  for i = 0 to (length s - 1) do
    let c = String.get s i in
    r := !r@[c]
  done; !r
;;

let is_vowel = function
  | 'a' -> true
  | 'o' -> true
  | 'y' -> true
  | 'e' -> true
  | 'u' -> true
  | 'i' -> true
  | _ -> false

let l = ( explode s 
  |> List.filter (fun x -> (not (is_vowel x)) )
  );;

List.iter (fun x -> print_string ("."^(String.make 1 x)) ) l; print_endline "";;
