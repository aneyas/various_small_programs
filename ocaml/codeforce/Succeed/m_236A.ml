open Scanf
open Printf

let name = Scanf.scanf "%s " ( fun x -> x)

let a = Array.make 27 0

let add_count c =
  let i = (Char.code c) - (Char.code 'a')
  in a.(i) <- a.(i) + 1

let () =
  String.iter add_count name

let flag = ref true

let flip c = 
  if (c>0) then flag := (not (!flag))


let () = 
  Array.iter flip a

let () =
  if (!flag) then print_endline "CHAT WITH HER!"
  else
    print_endline "IGNORE HIM!"




