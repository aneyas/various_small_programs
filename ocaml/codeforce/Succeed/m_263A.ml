open Printf

exception Found

let r = ref 0

let c = ref 0

let () =
  try 
    for i = 1 to 5 do
      for j = 1 to 5 do
        let n =  Scanf.scanf "%d " ( fun x -> x) in
        if(n==1) then begin r:=i; c:=j; raise Found end
      done
    done
  with Found -> ();;


let () = printf "%d \n" ( abs(!r-3) + abs(!c-3) )
