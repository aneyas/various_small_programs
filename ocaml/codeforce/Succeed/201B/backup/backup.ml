let rec lcs_no_virus s1 s2 virus = 
  let open LCS in
  match (contain_virus s1 virus, contain_virus s2 virus) with
    | (false, false) 
    | (true, false)
    | (false, true)  -> string_of_list ( lcs (list_of_string s1)
                                           (list_of_string s2))
    |(true,true) -> lcs_no_virus (remove_virus s1 virus) (remove_virus
                                                            s2 virus)
