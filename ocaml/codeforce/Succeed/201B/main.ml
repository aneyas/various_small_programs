open Scanf;;
open String;;

let longest xs ys = if List.length xs > List.length ys then xs else
    ys;;
let str_longset xs ys = if String.length xs > String.length ys then xs
  else ys;;

module LCS =
struct
  
let rec lcs_rec a b = match a, b with
    [], _
  | _, []        -> []
  | x::xs, y::ys ->
    if x = y then
      x :: lcs_rec xs ys
    else 
      longest (lcs_rec a ys) (lcs_rec xs b)
;;


  let lcs xs' ys' =
    let xs = Array.of_list xs'
    and ys = Array.of_list ys' in
    let n = Array.length xs
    and m = Array.length ys in
    let a = Array.make_matrix (n+1) (m+1) [] in
    for i = n-1 downto 0 do
      for j = m-1 downto 0 do
        a.(i).(j) <- if xs.(i) = ys.(j) then
            xs.(i) :: a.(i+1).(j+1)
          else
            longest a.(i).(j+1) a.(i+1).(j)
      done
    done;
    a.(0).(0);;

  let list_of_string str =
    let result = ref [] in
    String.iter (fun x -> result := x :: !result)
      str;
    List.rev !result;;
  
  let string_of_list lst =
    let result = String.create (List.length lst) in
    ignore (List.fold_left (fun i x -> result.[i] <- x; i+1) 0 lst);
    result

end
;;

let (s1,s2,virus) = Scanf.bscanf Scanf.Scanning.stdin "%s %s %s "
    (fun x y z  ->  (x,y,z) )  in
let open LCS in
let s =string_of_list ( lcs (list_of_string s1) (list_of_string s2))
in
print_string s; print_endline "";;
