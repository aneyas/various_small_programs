(*Binary Index Tree*)
open Scanf;;
open String;;
open List;;
open Str;;

let (s1,s2,virus) = Scanf.bscanf Scanf.Scanning.stdin "%s %s %s "
    (fun x y z  ->  (x,y,z) ) ;;

let vl = String.length virus;;

let n = String.length s1;;
let  m = String.length s2;;
let a = Array.make_matrix (n+1) (m+1) [];;

type sList = string list;;
let print_sList rl = (*iter (fun x -> print_string x; print_string "
                       ")  rl;;*)
  match rl with
    [] -> print_endline "0"
    |hd :: _ -> let x = longest rl 
      in  print_string x; print_endline ""
;;


(*If string s1 contains string s2*)
let re = Str.regexp_string virus;;

let contains x =
  try ignore (Str.search_forward re x 0); true
  with Not_found -> false
;;

(*Make sure a list contains unique elements*)
let rec uniq x =
  let rec uniq_help l n = 
    match l with
    | [] -> []
    | h :: t -> if n = h then uniq_help t n else h::(uniq_help t n) in
  match x with
  | [] -> []
  | h::t -> h::(uniq_help (uniq t) h)
;;

let sList_merge c  i j  = 
   (*print_string cs; print_string " "; print_sList sL; print_endline "";
    print_sList ( List.map (fun x -> cs^x) sL );*)
  begin
    let sL = a.(i).(j) in
      match sL with 
        |[] ->  if not (contains c) then [c] else []
        |_ -> let rslt =  List.map (fun x -> c^x) sL in
        let rslt2 = List.filter (fun x -> not (contains x) ) rslt in 
          match rslt2 with 
            [] ->List.filter (fun x -> not (contains x) ) (
              uniq(List.append 
                     (List.append 
                      (List.map (fun x -> c^x) a. (i+1).(j))
                      (List.map (fun x -> c^x) a. (i).(j+1))    
                     )
                     a.(i).(j)
                   )
            )
        |_ ->rslt2
  end
;;

let sList_merge2 sL1 sL2 = match sL1, sL2 with
  |([],[]) -> []
  |([],  x) -> x
  |( x, []) -> x
  |(x, y) -> uniq ( List.append x y )
;;
    
(*Find the LCS, record them all*)
let lcs contains =
  for i = n-1 downto 0 do
    for j = m-1 downto 0 do
      a.(i).(j) <- if (String.get s1  i) = (String.get  s2 j) then
          let cs = String.sub s1 i 1 in
          sList_merge  cs  (i+1) (j+1) 
        else
          sList_merge2  a.(i).(j+1) a.(i+1).(j)
    done
  done;
  a.(0).(0)
;;


let longest xs ys = if List.length xs > List.length ys then xs else
    ys;;

let lcs2 xs' ys' =
  let xs = Array.of_list xs'
  and ys = Array.of_list ys' in
  let n = Array.length xs
  and m = Array.length ys in
  let a = Array.make_matrix (n+1) (m+1) [] in
  for i = n-1 downto 0 do
    for j = m-1 downto 0 do
      a.(i).(j) <- if xs.(i) = ys.(j) then
          xs.(i) :: a.(i+1).(j+1)
        else
          longest a.(i).(j+1) a.(i+1).(j)
    done
  done;
  a.(0).(0);;

let list_of_string str =
  let result = ref [] in
  String.iter (fun x -> result := x :: !result)
    str;
  List.rev !result;;

let string_of_list lst =
  let result = String.create (List.length lst) in
  ignore (List.fold_left (fun i x -> result.[i] <- x; i+1) 0 lst);  result
;;

let remove_char x r = 
    if r = 0 then
     String.sub x 1 ((String.length x) -1)
    else  
      (String.sub x 0 r)^(String.sub x (r+1)
                                   ((String.length x) - r-1)) 
;;

let rec return_longest xl = 
  match xl with
    [] -> ""
  |hd :: tl ->let xxl = return_longest tl in
    if (String.length hd )> (String.length xxl) then
      hd 
    else xxl
;;

let rec remove x =
  try 
    let r = Str.search_forward re x 0 in
    let basket = ref [] in
    for i = 0 to vl - 1 do
      let newx = remove_char x (r+i) in
      basket := List.append !basket [(remove newx)]
    done;
    return_longest !basket
  with Not_found -> x
;;


if s1=s2 then 
  begin
    print_string (remove s1); print_endline ""
  end
else
  begin
    let s =string_of_list ( lcs2 (list_of_string s1) (list_of_string s2))
    in
    if  not (contains s) then 
      begin
        print_string s; print_endline "";
      end
    else
      let rslt = lcs contains in
      print_sList rslt
  end
