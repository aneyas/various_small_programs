(**D. The Child and Sequence*)

open Scanf;;
open Num;;
open Printf;;
open Array;;

let n = Scanf.bscanf Scanf.Scanning.stdin "%d " ( fun x -> x);;

let a = Array.create n 0;;

let total = ref (num_of_int 0);;
for i = 0 to n-1 do
  let ni = Scanf.bscanf Scanf.Scanning.stdin "%d " ( fun x -> x) in
  a.(i) <- ni; total:=!total +/ (num_of_int ni)
done
 ;;

if n=1 then
  printf "%s\n" (string_of_num !total)
else
  begin

    let () = Array.fast_sort (fun x y -> x-y) a in
    (*Array.iter(fun i -> printf "%d\n" i) a;*)
    let rec cal_score m pn score total_v =
      match m with
      | 2 -> total_v +/ score
      | _ ->cal_score (m-1) (pn+1) (score+/total_v) (total_v -/
                                                       (num_of_int
                                                          a.(pn)) )
    in 
    let rslt = cal_score n 0 (num_of_int 0)  !total
    in
    print_string (string_of_num  (rslt +/  !total) );
                  print_endline "";
  end
;;
