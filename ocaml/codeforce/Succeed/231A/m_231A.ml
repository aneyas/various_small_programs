let read_int () = Scanf.scanf "%d " (fun x -> x);;
let read_int3 () = Scanf.scanf " %d %d %d " (fun x y z -> (x,y,z) );;

let n = read_int ();;

let a = Array.init n (fun i -> (read_int3 () ));;

let vote = function
  | (0,0,0) -> 0
  | (1,0,0) -> 0
  | (0,1,0) -> 0
  | (0,0,1) -> 0
  | (_,_,_) -> 1
;;

let b = Array.map (fun x -> vote x) a
        |> Array.fold_left (+) 0
    in print_int b; print_endline ""
