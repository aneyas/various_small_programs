open Scanf;;
open String;;

type cell = RC | NRC;;

let print_cell t = match t with
    RC -> print_endline "RC"
   |NRC -> print_endline "NRC"

let n = Scanf.bscanf Scanf.Scanning.stdin "%d " (fun x -> x);;

let ntostring=[|"A";"B";"C";"D";"E";"F";"G";"H";"I";"J";"K";"L";"M";"N";"O";"P";"Q";"R";"S";"T";"U";"V";"W";"X";"Y";"Z"|];;

let myindex s c = try index s c with Not_found -> -100;;

let is_digit = function
    '0'..'9' -> true
  |_ -> false;;

let  rec digit_index x ind =
     let len = length x in
     match len with
     | 0 -> ind
     | _  -> match get x 0 with
       | '0' .. '9' -> ind
       |_ -> digit_index (sub x 1 (len - 1) ) (ind + 1)
;;
let rec process_RC x = 
  let (rown,coln) = get_num x in
  print_string (convert_to_AB coln); print_string (string_of_int rown); print_endline "";
and get_num x = let m = index x 'C' in
  let tmp = (length x) - m -1 in
  let tmp1 = match tmp with
    | x when x <=0 -> 1
    |_ -> int_of_string (sub x (m+1) tmp)
  in
  (int_of_string (sub x 1 (m-1)), tmp1)
and convert_to_AB x = 
  let y = ref x in
  let ss = ref "" in
  let () = 
    while !y > 0  do
      let tmp = ((!y-1) mod 26) in
      y := (!y - tmp - 1) / 26;
      ss := ntostring.(tmp) ^ !ss
    done in !ss;;

let rec process_NRC x = 
  let (ab,row) = get_content x in
  print_char 'R'; print_string (string_of_int row); print_char 'C';
  print_string(string_of_int  (convert_to_int ab));  
  print_endline "";
and get_content x =
  let m = digit_index x 0 in
  let tmp=(length x) -m in
  let tmp2 = match tmp with
    |0 -> 1
    |_ -> int_of_string (sub x m tmp) in
  (sub x 0 m, tmp2)
and convert_to_int x = 
  let k = ref ((length x) - 1) in
  let mul = ref  1 in
  let acc = ref  0 in
  let nA = int_of_char 'A' in
  let () =
    while !k >=0 do
      let f = get x !k in
      let g = (int_of_char f) - nA + 1 in
      acc := !acc +  g*(!mul); mul:= !mul* 26; k:=!k-1
    done in !acc
;;

let  process t s = match t with
    RC -> process_RC s
   |NRC -> process_NRC s
;;

for i=1 to n do
  let ms = ref (String.make 32 'A') in
  let () = 
    ms  := Scanf.bscanf Scanf.Scanning.stdin "%s " (fun x -> x) 
  in
  let t = match (myindex !ms 'R', myindex !ms 'C')  with
    | (-100, _) -> NRC
    | (_,-100) -> NRC
    | (a, b)  -> if  b - a > 1 && (is_digit  (get !ms (a+1))) then RC else NRC
  in (*print_string ! ms; print_string " :  ";*) process t !ms
done;; 

