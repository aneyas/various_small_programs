let read_int () = Scanf.scanf "%d " (fun x -> x);;
let read_int3 () = Scanf.scanf " %d %d %d " (fun x y z -> (x,y,z) );;

let n = read_int ()

type action = Rest | Test | Sport | None;;
type day = A | B | C | D;; (* 0, 1, 2, 3*)

let itoa = function
  | 0 -> A
  | 1 -> B
  | 2 -> C
  | 3 -> D

let a = Array.init n (fun i -> (read_int () |> itoa))

let act_poss d act =
  match (d, act) with
  | (A, Sport) -> false
  | (A,  Test) -> false
  | (B, Sport) -> false
  | (C,  Test) -> false
  | (_,     _) -> true

type rmin = { mutable rest : int; mutable sport : int; mutable test : int};;

  (*Note: should not use Array.make here*)
let r = Array.init (n+1) (fun i ->
                     {rest = max_int; sport = max_int; test = max_int });;

let () = r.(0) <- {rest = 0; sport = 0; test = 0};;

let () =
  for i = 1 to n
  do
    let x = r.(i-1).sport in
    let y = r.(i-1).test in
    let z = r.(i-1).rest in
    begin
      if (act_poss a.(i-1) Rest) then
        let w = min x (min y z) in
        if(w<max_int) then r.(i).rest <- (w+1);
      if (act_poss a.(i-1) Sport) then r.(i).sport<- (min y z);
      if (act_poss a.(i-1) Test) then  r.(i).test <- (min x z)
    end;
    (* printf "-- %d %d %d\n" r.(i).rest r.(i).sport r.(i).test *)
  done;;

let () =  Printf.printf "%d\n" ( min r.(n).test (min r.(n).rest r.(n).sport ));;
