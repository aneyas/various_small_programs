#include <stdio.h>
#include <math.h>


const int N = 24;

int 
dec_num(int* d, int n){
  int rslt = d[0];
  int seven_plus  = 7;
  for(int i = 1; i < n; i++){
    rslt += d[i] * seven_plus;
    seven_plus *=7;
  }
  return rslt;
}

int
next(int* d, int n, int h_limit){
  if( dec_num(d,n) >= h_limit - 1 ){
    return 0;
  }else{
    for(int i= 0; i< n; i++){
      if(d[i] < 6){
        d[i]++;
        break;
      }else{
        d[i]=0;
      }
    }
  }
  return 1;
}


int
is_valid(int* d, int n){
  int flag[7];
  for(int i=0; i<7; i++) flag[i] =0;

  for(int i = 0; i < n; i++){
    int x = d[i];
    if(flag[x]){
      return 0;
    }else{
      flag[x]= 1;
    }
  }

  return 1;
}

int
is_valid_2(int* d1, int* d2, int n1, int n2){

  for(int i = 0; i < n1; i++){
    for(int j = 0; j < n2; j++){
      if(d1[i]==d2[j]) return 0;
    }
  }

//  for(int i = 0; i < n1; i++){
//    printf("=%d ", d1[i]);
//  }
//  printf("\n");
//
//  for(int i = 0; i < n2; i++){
//    printf("=%d ", d2[i]);
//  }
//  printf("\n");

  return 1;
}

int
main(){
  int hour[N];
  int minute[N];

  int n,m;
  scanf("%d %d", &n, &m);

  int ndd = ceil(log(n) / log(7) );
  int ndm = ceil(log(m) / log(7) );

  if(ndd == 0) ndd++;
  if(ndm == 0) ndm++;

  //Debug
  //printf("--%d %d\n",ndd,ndm);
  //return 0;

  if(ndd+ndm>7){
    printf("%d\n",0);
    return 0;
  }

  //Now ndd and ndm are really small, I will use brute force.
  int count = 0;

  for(int i=0; i< ndd; i++) hour[i] = 0;

  //Debug
  //int d[] = {6,6,6,6,6};
  //int x = dec_num(d,5);
  //printf("?? %d\n",x);
  
//  do {
//    int x = dec_num(hour,ndd);
//      for(int i=0; i< ndm; i++) minute[i] = 0;
//      do {
//        int y = dec_num(minute,ndm);
//        printf(">>%d %d\n", x,y);
//      }while( next(minute,ndm, m) );
//  }while( next(hour,ndd,n) );

  do{
    if( is_valid(hour,ndd) ){
      for(int i=0; i< ndm; i++) minute[i] = 0;
      do{
        if( is_valid(minute,ndm) ){
          if( is_valid_2(hour,minute, ndd,ndm) ){
            count++;
//            int x, y;
//            x = dec_num(hour,ndd);
//            y = dec_num(minute,ndm);
//            printf("--%d %d\n", x,y);
          }
        }
      }while( next(minute,ndm, m) );
    }
  }while( next(hour,ndd,n) );

  printf("%d\n",count);
  return 0;
}
