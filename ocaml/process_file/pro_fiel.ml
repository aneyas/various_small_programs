open Core.Std;;

let sum_file1 filename =
    In_channel.with_file filename ~f:(fun file ->
        let numbers = List.map ~f:Int.of_string 
