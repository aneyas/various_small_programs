(** Monte Carlo **)

open Scanf;;
open Num;;
open Printf;;
open Array;;
open Big_int;;

let rand_update x = (431 * x + 5) mod 613233

let make_rand () = 
  let x = ref 11 in
  fun () -> begin x := rand_update !x; !x end
;;

(* Do some debugging 

let a = make_rand ();;
print_int (a ());;
print_endline;;
print_int (a ());;
print_endline;;
*)

let gcd a b = int_of_big_int 
(gcd_big_int (big_int_of_int a) (big_int_of_int b))

let rand = make_rand ()

let cesaro_test () = gcd (rand () ) (rand () )  = 1

let monte_carlo trials experiment =
  let rec iter trials_remain trials_passed =
    if trials_remain = 0 then
      float_of_int trials_passed /. float_of_int trials
    else if experiment () then
      iter (trials_remain - 1) (trials_passed + 1)
    else
      iter (trials_remain - 1) trials_passed
  in
  iter trials 0

let estimate_pi trials =
  sqrt 6. /. (monte_carlo trials cesaro_test)


let x = estimate_pi 100000 in
print_float x;;
