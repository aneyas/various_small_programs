(*
 * To compile.
ocamlfind ocamlc -linkpkg -thread -package core freq.ml -o freq.byte
 *)

open Core.Std

let built_counts () =
  In_channel.fold_lines stdin ~init:[] ~f:Counter.touch

let () =
  built_counts ()
      |> List.sort ~cmp:(fun (_,x) (_,y) -> Int.descending x y)
      |> (fun l -> List.take l 100)
      |> List.iter ~f:(fun (line, count) -> printf "%3d: %s\n" count line)
