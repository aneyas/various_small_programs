open Owl;;

let x = Dense.Ndarray.D.zeros [|5;5|];;

(* same as Arr.zeros [|5;5|] *)

let y = Dense.Matrix.D.zeros 5 5;;
