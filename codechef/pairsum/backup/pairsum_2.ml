(** We call an array of integers X of size N good if it can be partitioned into
 * 2 arrays of size n/2, say p1[ ] and p2[ ], such that p1[0] + p2[0] = p1[1] +
 * p2[1] = p1[2] + p2[2] = ... . Given an array Y, determine the size of its
 * largest subset which is a good array.
Input

The first line contains the integer N (≤ 100).  The next line contains N space
separated integers, which are the elements of the array X.  Output

One number which is the size of the greatest subset as mentioned in the problem
statement.  Example

Input: 6

1 4 2 3 8 10 

Output: 4 
Explanation: The array {1,4,2,3} is good, since you can
form p1[] = {1,2} and p2[] = {4,3}. (satisfying 1 + 4 = 2 + 3)
*)

(*open Printf;;*)
(*open Scanf;;*)
(*open List;;*)

let n = read_int ();;


let rec loop m = 
  let s = read_string () in
  if m=0 then begin
  end
  else begin
    printf "%s\n" s;
    loop m - 1
  end 
in
loop n

(*let q = List.map int_of_string split;;

let myprint x = begin
  print_int x;
  print_newline ();
end;; 

List.map myprint q;;
print_newline ();;
*)

(*
let rec f lst rslt =
  match lst with
  | [] -> rslt
  | hd :: tl -> let tll = List.map (fun x -> x + hd ) tl 
  in f tl (List.append rslt tll);;

let accu = f q [];;
(*List.map myprint accu;;*)

let rec count a ll cnt =
  match ll with
  | [] -> cnt
  | hd :: tl -> if a = hd then count a tl (cnt + 1) else count a tl cnt;;


let rec mfreq l n =
  match l with
  | [] -> n
  | hd :: tl -> let m = (count hd tl 1) and x = (mfreq tl n) in 
  if m >= x then m else x;;

let rslt = mfreq accu 0 in
if rslt=1 then begin
  print_int 0;
  exit 0
end
else begin
  print_int (2 * rslt);
  exit 0
end;;
*)

(**TODO: the largest frequency of numbers not in the same row or column.*)
