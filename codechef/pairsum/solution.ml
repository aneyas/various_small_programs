open Scanf;;

let n = Scanf.scanf "%d " (fun x-> x);;


let p = ref [];;

for i = 0 to n - 2 do
  p :=  Scanf.scanf "%d " (fun x-> x) :: !p
done;;

if n >=1 then p :=  Scanf.scanf "%d" (fun x-> x) :: !p;;

let q = List.rev !p;;

let myprint x = 
  print_int x; print_newline ()
;; 

List.map myprint q;;
print_newline ();;

let rec f lst rslt =
  match lst with
  | [] -> rslt
  | hd :: tl -> let tll = List.map (fun x -> x + hd ) tl 
  in f tl (List.append rslt tll);;

let accu = f q [];;
(*List.map myprint accu;;*)

let rec count a ll cnt =
  match ll with
  | [] -> cnt
  | hd :: tl -> if a = hd then count a tl (cnt + 1) else count a tl cnt;;


let rec mfreq l n =
  match l with
  | [] -> n
  | hd :: tl -> let m = (count hd tl 1) and x = (mfreq tl n) in 
  if m >= x then m else x
;;

let rslt = mfreq accu 0 in
if rslt=1 then 
    print_int 0
  else print_int (2 * rslt)
;;

(**Solution
 * loop through the array with index i, and j
 * if a[i]+a[j] did not appear yet, then add 1 to the container A indexed by a[i]+a[j]
 *        and add i, j to container B indexed by a[i]+a[j]
 * if a[i]+a[j], check if i or j already in B(a[i]+a[j]), if so, cycle,
 *      if not add 1 to A(a[i]+a[j])
 *
 * Find the largest value in A(a[i]+a[j])
 *)

(**TODO: the largest frequency of numbers not in the same row or column.*)
(** We call an array of integers X of size N good if it can be partitioned into
 * 2 arrays of size n/2, say p1[ ] and p2[ ], such that p1[0] + p2[0] = p1[1] +
 * p2[1] = p1[2] + p2[2] = ... . Given an array Y, determine the size of its
 * largest subset which is a good array.
Input

The first line contains the integer N (≤ 100).  The next line contains N space
separated integers, which are the elements of the array X.  Output

One number which is the size of the greatest subset as mentioned in the problem
statement.  Example

Input: 6

1 4 2 3 8 10 

Output: 4 
Explanation: The array {1,4,2,3} is good, since you can
form p1[] = {1,2} and p2[] = {4,3}. (satisfying 1 + 4 = 2 + 3)
*)


