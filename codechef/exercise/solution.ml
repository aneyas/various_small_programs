(*
let () =
  try
    while true do
      let line = input_line stdin in
      let size = String.length line in
      Printf.printf "%d\n" size
    done
  with End_of_file -> ()
*)

(*
let a = [|1;2;3|];;

Array.map (myPrint) a;;

let b = Array.append a [|10|];;
Array.map (myPrint) b;;
*)

(*
let line_stream_of_channel channel =
  Stream.from
  (fun _ -> try Some (input_line channel) with End_of_file -> None)

let lines =
  let xs = ref [] in
  Stream.iter
  (fun x -> xs := x :: !xs)
  (line_stream_of_channel stdin);
  List.rev !xs;;

let myPrint a = Printf.printf "%s\n" a;;
List.map (myPrint) lines;
*)

let int_stream_of_channel channel =
  let buffer = (Scanf.Scanning.from_channel channel) in
  Stream.from
  (fun count ->
    try
      match Scanf.bscanf buffer " %d " (fun x -> x) with
      | None -> None
      | s -> Some s
    with End_of_file -> None);;

let lines =
  let xs = ref [] in
  Stream.iter
  (fun x -> xs := x :: !xs)
  (int_stream_of_channel stdin);
  List.rev !xs;;

let myPrint a = Printf.printf "%d\n" a;;
List.map (myPrint) lines;



