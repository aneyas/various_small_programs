open Scanf;;
open Array;;

let read_int () =  Scanf.scanf "%d " ( fun
                                                             x->x);;
let (n,m) =  Scanf.scanf "%d %d " ( fun x y -> (x,y));;

let a = Array.make (n) 0;;
for i = 0 to n-1 do
  let x = read_int () in
  a.(i) <- x
done;;


(*
Array.iter (fun x -> print_int x; print_endline "") a;;
*)

let stops = Array.make (n) max_int;;
let paths = Array.make (n) 0;;

(*Initial value*)

exception Oil_negative;;

stops.(0) <- 0;;

let last_refill = ref 0 in
let next_refill = ref 1 in
while !last_refill < n do
  (*  
print_int !last_refill; print_string " "; print_int !next_refill; 
   *) 
 while !next_refill < n && (a.(!next_refill) - a.(!last_refill) <= m) do
    stops.(!next_refill) <- min stops.(!next_refill) (stops.(!last_refill)+1);
    next_refill:=!next_refill+1
  done;
(* print_string "-- "; print_int !next_refill;print_endline "";
 *)  
for i = !last_refill+1 to (!next_refill-1) do (*suppose we refill at i *)
    let j = ref (!next_refill) in
    while !j< n && a.(!j)-a.(i) <= m do
      stops.(!j) <- min (stops.(i)+1) stops.(!j);
      j:=!j+1
    done
  done;

  last_refill := !next_refill;
 
done
;;

(*				  
print_int stops.(n-1);;
print_endline "";;
 *)

let rslt1= (stops.(n-1)-1) mod 1000000007 in
print_int  rslt1;;

print_endline "";;

(*Array.iter (fun x -> print_int x; print_string " ") least_num_stops;;*)

