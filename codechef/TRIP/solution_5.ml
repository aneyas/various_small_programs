(*Sep 9, 2014; The simplest version for TRIP*)

open Scanf;;
open Array;;

let read_int () =  Scanf.scanf "%d " ( fun
                                                             x->x);;
let (n,m) =  Scanf.scanf "%d %d " ( fun x y -> (x,y));;

let a = Array.make (n) 0;;
for i = 0 to n-1 do
  let x = read_int () in
  a.(i) <- x
done;;


(*
Array.iter (fun x -> print_int x; print_endline "") a;;
*)

let stops = Array.make (n) max_int;;
let paths = Array.make (n) 0;;

(*Initial value*)

stops.(0) <- 0;;

let last_refill = ref 0 in
let next_refill = ref 1 in
while !last_refill < n-1 do
  
(*print_int !last_refill; print_string " "; print_int !next_refill; 
 *)  
 while !next_refill < n && (a.(!next_refill) - a.(!last_refill) <= m) do
    stops.(!next_refill) <- min stops.(!next_refill) (stops.(!last_refill)+1);
    next_refill:=!next_refill+1
  done;

(*print_string "-- "; print_int !next_refill;print_endline "";
 *) 

(*
 let i = (!next_refill-1) in (*suppose we refill at i *)
 let j = ref (!next_refill) in
 while !j< n && a.(!j)-a.(i) <= m do
   stops.(!j) <- min (stops.(i)+1) stops.(!j);
   j:=!j+1
 done;
 
 last_refill := !next_refill;
 *)
last_refill :=!next_refill -1;
 
done
;;

if n = 1 then
  begin
    print_int  1; print_string " "; print_int 1; print_endline ""
  end
else
  let rslt1= (stops.(n-1)-1) mod 1000000007 in
  let rslt2 = paths.(n-1) in
  print_int  rslt1; print_string " "; print_int rslt2;
  print_endline ""
;;
