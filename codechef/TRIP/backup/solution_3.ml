
open Scanf;;
open Array;;

let read_int () =  Scanf.scanf "%d " ( fun
                                                             x->x);;
let (n,m) =  Scanf.scanf "%d %d " ( fun x y -> (x,y));;

let a = Array.make (n+1) 0;;
for i = 0 to n-1 do
  let x = read_int () in
  a.(i) <- x
done;;
a.(n) <- a.(n-1);;


(*
Array.iter (fun x -> print_int x; print_endline "") a;;
*)

let least_num_stops = Array.make (n+1) max_int;;
let num_of_path = Array.make (n) 0;;

(*Initial value*)

exception Oil_negative;;

least_num_stops.(0) <- 1;;

let rec loop i stops_so_far oil =
 (* print_int i; print_string " "; print_int stops_so_far; print_string " ";  print_int oil; print_string " ||  ";
  Array.iter (fun x -> print_int x; print_string " ") least_num_stops;
  print_endline "";*)
(*  if i = n -1 then
    begin
      if oil >= a.(i) - a.(i-1)  then
        least_num_stops.(i)  <- min least_num_stops.(i)
          stops_so_far
      else 
        least_num_stops.(i)  <- min least_num_stops.(i)
          (stops_so_far+1)
    end

  else*) 

  if (i< n) then
    begin 
      let oil2 = oil - (a.(i+1) - a.(i) )in 
      if oil2 <0 then 
        begin
          (*must fill oil*)
          least_num_stops.(i+1) <- min ( stops_so_far + 1)
            least_num_stops.(i+1);
          loop (i+1) least_num_stops.(i+1)  (m - (a.(i+1) -a.(i)))
        end
      else 
        begin

          loop (i+1) stops_so_far  oil2 ; (*assume no stop at i*)
          loop (i+1)  (stops_so_far+1)  (m - (a.(i+1) - a.(i)));

          (*First consider no fill gas at i*)
          let j = ref (i+1) in
          while (!j<=n-1 &&  a.(!j) - a.(i) <= oil) do
            least_num_stops.(!j) <- min stops_so_far 
                least_num_stops.(!j);
            j:= !j+1
          done ;

          (*Now consider fill gas at i*)
          let j = ref (i+1) in
          while ( !j<=n-1 && a.(!j) - a.(i) <= m) do
            least_num_stops.(!j) <- min (stops_so_far + 1)
                least_num_stops.(!j);
            j:= !j+1
          done;
        end
    end
;;

let () = loop 0 1 m;;

let rslt1= (least_num_stops.(n-1)-1) mod 1000000007 in
print_int  rslt1;;

print_endline "";;

(*Array.iter (fun x -> print_int x; print_string " ") least_num_stops;;*)

