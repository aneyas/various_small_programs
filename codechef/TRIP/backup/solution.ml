open Scanf;;
open Array;;

let read_int () =  Scanf.scanf "%d " ( fun
                                                             x->x);;
let (n,m) =  Scanf.scanf "%d %d " ( fun x y -> (x,y));;

let a = Array.make (n+1) 0;;
for i = 0 to n-1 do
  let x = read_int () in
  a.(i) <- x
done;;
a.(n) <- a.(n-1);;


(*
Array.iter (fun x -> print_int x; print_endline "") a;;
*)

let least_num_stops = Array.make (n+1) max_int;;
let num_of_path = Array.make (n) 0;;

(*Initial value*)

exception Oil_negative;;

least_num_stops.(0) <- 0;;
let oil = ref m in 
for i = 1 to n-1 do
  let distance_1 = a.(i) -a.(i-1) in
  let distance_2 = a.(i+1)-a.(i) in
  oil := (!oil - distance_1); 
  if (!oil < 0) then raise Oil_negative
  else if distance_2 > !oil then (*must stop at i*) 
    begin
      least_num_stops.(i) <- min (least_num_stops.(i-1) + 1)
          least_num_stops.(i);
      oil := m (*Refill oil*)
    end
  else (*There is no need to stop at i; test all possibilities*)
    
done; 

let rslt1= (least_num_stops.(n-1)) mod 1000000007 in
print_int  rslt1;;

print_endline "";;

(*Array.iter (fun x -> print_int x; print_string " ") least_num_stops;;*)

