open Scanf;;
open Array;;

let read_int () =  Scanf.scanf "%d " ( fun
                                                             x->x);;
let (n,m) =  Scanf.scanf "%d %d " ( fun x y -> (x,y));;

let a = Array.make (n+1) 0;;
for i = 0 to n-1 do
  let x = read_int () in
  a.(i) <- x
done;;
a.(n) <- a.(n-1);;


(*
Array.iter (fun x -> print_int x; print_endline "") a;;
*)

let least_num_stops = Array.make (n+1) max_int;;
let num_of_path = Array.make (n) 0;;

(*Initial value*)

exception Oil_negative;;

least_num_stops.(0) <- 1;;
let oil = ref m in 
let i = ref 1 in
while !i < n do
  oil := !oil - (a.(!i) - a.(!i-1));
  if (!oil < 0 && !i>0) then 
    begin
      (*Have to stop at (i-1) *)
      least_num_stops.(!i-1)<- least_num_stops.(!i-1) + 1;
      (*Go back to i-1*)
      oil:=m-(a.(!i)-a.(!i-1))
    end
  else
    begin
      least_num_stops.(!i) <- least_num_stops.(!i-1); 
      (* We know we  cannot  stop at i here*)
      let j = ref (!i+1) in
      while ( !j<=n-1 && a.(!j) - a.(!i) <= !oil) do
        least_num_stops.(!j) <- min least_num_stops.(!i) 
            least_num_stops.(!j);
        j:= !j+1
      done ;
      i:=!i+1
    end
done; 

let rslt1= least_num_stops.(n-1) - 1 in
print_int  rslt1;;

print_endline "";;

(*Array.iter (fun x -> print_int x; print_string " ") least_num_stops;;*)

