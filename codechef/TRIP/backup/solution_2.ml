open Scanf;;
open Array;;

let read_int () =  Scanf.scanf "%d " ( fun
                                                             x->x);;
let (n,m) =  Scanf.scanf "%d %d " ( fun x y -> (x,y));;

let a = Array.make (n+1) 0;;
for i = 0 to n-1 do
  let x = read_int () in
  a.(i) <- x
done;;
a.(n) <- a.(n-1);;


(*
Array.iter (fun x -> print_int x; print_endline "") a;;
*)

let least_num_stops = Array.make (n+1) max_int;;
let num_of_path = Array.make (n) 0;;

(*Initial value*)

exception Oil_negative;;

least_num_stops.(0) <- 1;;
let oil = ref m in 
for i = 1 to n-1 do 
  oil := !oil - (a.(i) - a.(i-1));
  if (!oil < 0) then 
    (*Have to stop at (i-1) *)
  else
    begin
      if least_num_stops.(i) = least_num_stops.(i-1) then 
        (* We know we  cannot  stop at i here*)
        let j = ref (i+1) in
        while ( a.(!j) - a.(i) <= !oil  && !j<=n-1) do
          least_num_stops.(!j) <- min least_num_stops.(i) 
              least_num_stops.(!j);
          j:= !j+1
        done ;
      else
        (*Now consider stop at i*)
        let num_stop_at_i = least_num_stops.(i-1) + 1 in
        let j = ref (i+1) in
        while ( a.(!j) - a.(i) <= m  && !j <= n-1) do
          least_num_stops.(!j) <- min num_stop_at_i
              least_num_stops.(!j);
          j:= !j+1
        done;
        oil := m 

      end
done; 

let rslt1= least_num_stops.(n-1) - 1 in
print_int  rslt1;;

print_endline "";;

(*Array.iter (fun x -> print_int x; print_string " ") least_num_stops;;*)

