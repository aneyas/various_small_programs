
open Scanf;;
open Array;;

let read_int () =  Scanf.scanf "%d " ( fun
                                                             x->x);;
let (n,m) =  Scanf.scanf "%d %d " ( fun x y -> (x,y));;

let a = Array.init n (fun x-> read_int () ) ;;

(*
Array.iter (fun x -> print_int x; print_endline "") a;;
*)


let ways = Array.make n 0;;
let ans = Array.make n n;;

ans.(0)<-0;;
ways.(0) = 1;;
  
let i= ref 0 in
let j =ref (!i+1 ) in
let () = 
  for i=0 to n-1 do
    j:=i+1;
    while  !j<n && a.(!j) - a.(i) <=m  do
      if ans.(!j) = ans.(i)+1 then
        ways.(!j)<-ways.(!j)+1
      else if  ans.(i) +1 < ans.(!j) then
        begin
          ans.(!j) <- ans.(i)+1; 
          ways.(!j)<-ways.(i)
        end;
      j:=!j+1
    done;
  done
in  

print_int  (ans.(n-1)-1); print_string " ";
print_int (ways.(n-1) mod 1000000007);
print_endline "";;

(*Array.iter (fun x -> print_int x; print_string " ") least_num_stops;;*)

