/*
 * =====================================================================================
 *
 *       Filename:  1.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  09/07/2014 05:17:30 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */

#include<stdio.h>
#include<stdlib.h>
inline int next_int() {
  int n = 0;
  char c = getchar();
  while (!('0' <= c && c <= '9')) {
    c = getchar();
  }
  while ('0' <= c && c <= '9') {
    n = n * 10 + c - '0';
    c = getchar();
  }
  return n;
}
int main()
{
  int m,n,i;
  n=next_int();
  m=next_int();
  int *a=(int *)malloc(n*sizeof(int));
  int *ways=(int *)malloc(n*sizeof(int));
  int *ans=(int *)malloc(n*sizeof(int));
  for(i=0;i<n;i++)
    a[i]=next_int();
  for(i=0;i<n;i++)
    ans[i]=n;
  ans[0]=0;
  ways[0]=1;
  i=0;//i points to the current location
  int j=i+1;//j points to the location just after i
  while(i<=n-1)
  {while(a[j]-a[i]<=m)
    {if(ans[j]==ans[i]+1)
      ways[j]++;
      else if(ans[i]+1<ans[j])
      {ans[j]=ans[i]+1;
        ways[j]=ways[i];}
      j++;
    }
    i++;
    j=i+1;
  }
  printf("%d %d\n",ans[n-1]-1,ways[n-1]%1000000007);
  return 0;}
  }
}
}
