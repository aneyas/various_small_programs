#include<stdlib.h>
#include<stdio.h>


long long int  ar[1000001],cways[1000001],ways[1000001];
int i,j,k,n,m,stops[1000001],mod=(int)(1e9+7);
int main(){
    scanf("%d%d",&n,&m);
    for(i=0;i<n;i++) scanf("%lld",&ar[i]);
    if(n==1){
      puts("0 1");
      return 0;
      }

       
    /* ways[n-1]=1;cways[n-1]=1;j=n-1;k=0; */
    /* ///ways[i] is the number of ways to reach destination using minimum number of stops. */
    /* ///cways[i] is the cummulative of ways. */
    /* ///ways[i] will be equal to [  sum of ways[j] for which (stops[j]==(stops[i]-1)) and it can be reached within M units] */
    /* ///The smallest index j for any index i ,such that stops[j]=stops[i]-1 ,is stored in K . */
    /* ///So the answer ways[i] = sum of ways[j] from j=k to index reachable within M units(stored in j). */
       /********
	  for(i=n-2;i>=0;i--){
	  while(ar[j]-ar[i]>m) j--;
	  stops[i]=stops[j]+1;
        if(stops[i]!=stops[i+1]) k=i+1;
        ways[i]=(cways[k]-cways[j+1]+mod)%mod;
        cways[i]=(cways[i+1]+ways[i])%mod;
    }
    */

    ways[0]=1; cways[0]=1;
    j=1; k=n-1;
    for (i=0; i<=n-2; i++){
	while(j<=n-1 && ar[j]-ar[i]<=m) j++;
	if(ar[j]-ar[i]>m)  stops[j] = stops[i]+1; (*Increase the stops by 1*)
	
	if ( stops[i] != stops[i+1] ) k=i;
	ways[i+1] = (cways[k]-cways[j-1]+mod)%mod;

	cways[i+1] = (cways[i]+ways[i])%mod;
    }


    printf("%d %llu\n",stops[n-1]-1,ways[n-1]);
    return 0;
}
