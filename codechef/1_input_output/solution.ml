open Int32;;
open Printf;;

let rec loop () = 
  let s = Scanf.scanf "%d " (fun x-> x) in
  if s=42 then begin
  end
  else begin
    printf "%d\n" s;
    loop ()
  end 
in
loop ()

(* An earlier version
let rec loop () = 
  let s = read_int () in
  if s=42 then begin
  end
  else begin
    print_int s; 
    print_newline ();
    loop ()
  end 
in
loop ()
*)

(** The problem 
 * Your program is to use the brute-force approach in order to find
 * the Answer to Life, the Universe, and Everything. More precisely... rewrite
 * small numbers from input to output. Stop processing input after reading in
 * the number 42. All numbers at input are integers of one or two digits.
Example


Input:
1
2
88
42
99

Output:
1
2
88
*)
