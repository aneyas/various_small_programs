(* This program generates k poinst for QE phonon calcualtion
 * Fri May 23 16:16:14 CST 2014
 * 
 * This program is developed by Dawei Wang (rand_wang@163.com)
 *
 * ALL RIGHTS RESERVED
 *  
 *)

open Core.Std

let createList n =
    let rec helper m rslt = 
        match m with
    | 0 -> rslt
    | x when x > 0 -> helper (m - 1) (List.append rslt [m])
   in 0 :: List.rev (helper n [])

let kpoints n k1 k2 =
    let dk = Array.map2_exn ~f:(fun x y -> (x -. y) /. (Int.to_float n) ) k2 k1 in
    let mylist = List.map ~f:Int.to_float (createList n) in
    let ndk = List.map ~f:(fun x -> Array.map2_exn ~f:(fun x y -> x +. y) k1 (Array.map ~f:(fun y -> x *. y) dk)) mylist in
    List.iter ~f:(fun x -> Array.iter ~f:(printf "%f ") x; printf "\n" ) ndk

let rec generate_them n l =
    match l with
    | [ ]  -> None
    | [_] -> None
    | k1 :: k2 :: tl -> 
            kpoints n k1 k2;
            generate_them n (k2 :: tl)

    
let special_ks = [[|0.;0.;0.|]; [|1.0;0.0;0.0|]; [|1.0;1.0;1.0|]];;

generate_them 10 special_ks
