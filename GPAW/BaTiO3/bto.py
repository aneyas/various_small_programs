import numpy as np
from ase import Atoms
from ase.units import Bohr
from gpaw import GPAW, PW

a=7.312 * Bohr
bto=Atoms('BaTiO3',
        positions=[(0.5*a,0.5*a,0.5*a),
                   (0.0*a,0.0*a,0.0*a),
                   (0.5*a,0.0*a,0.0*a),
                   (0.0*a,0.5*a,0.0*a),
                   (0.0*a,0.0*a,0.5*a)],
        cell=[a,a,a],pbc=[1,1,1])

calc = GPAW(mode=PW(200),
        xc='PBE',
        kpts=(4,4,4),
        txt='bto_gs.txt'
        )

bto.calc = calc
bto.get_total_energy()
#calc.write('bto_gs.gpw')

bto_R3m=Atoms('BaTiO3',
        positions=[(0.5*a,0.5*a,0.5*a),
                   (0.05*a,0.05*a,0.05*a),
                   (0.5*a,0.0*a,0.0*a),
                   (0.0*a,0.5*a,0.0*a),
                   (0.0*a,0.0*a,0.5*a)],
        cell=[a,a,a],pbc=[1,1,1])

calc2 = GPAW(mode=PW(200),
        xc='PBE',
        kpts=(4,4,4),
        txt='bto_R3m_gs.txt'
        )

bto_R3m.calc = calc2
bto_R3m.get_total_energy()
#calc.write('bto_R3m_gs.gpw')
