class Logger(object):
    """
    A sigleton example for logging files.
    """

    class __SingletonObject():
        def __init__(self, filename):
            self.filename = filename

        def __str__(self):
            return "{0!r} {1}".format(self, self.filename)

        def _write_log(self, level, msg):
            with open(self.filename, "a") as log_file:
                log_file.write("[{0}] {1}\n".format(level, msg))

        def critical(self, msg):
            self._write_log("Critical", msg)

        def error(self, msg):
            self._write_log("Error", msg)

        def warn(self, msg):
            self._write_log("Warn", msg)

    instance = None

    def __new__(cls, filename):
        if not Logger.instance:
            Logger.instance = Logger.__SingletonObject(filename)
        return Logger.instance

    def __getattr__(self, name):
        return getattr(self.instance, name)

    def __setattr__(self, name):
        return setattr(self.instance, name)


obj1 = Logger("test")
obj1.val = "Object value 1"
print("print obj1: ", obj1.filename)

print("----")
obj2 = Logger("test2")  # Cannot re-instantiate the 'Logger anywhere' in the project.
obj2.val = "Object value 2"
print("print obj1: ", obj1)
print("print obj2: ", obj2)

obj1.critical("hahaha, this is a critical test!")
obj2.warn("hahaha, this is a warn test!")
