def null_decorator(func):
    return func


def uppercase(func):
    def wrapper():
        rslt = func()
        return rslt.upper()

    return wrapper


# The decorator is the same as calling another function.
# @null_decorator
@uppercase
def greet():
    return "Hello!"


if __name__ == "__main__":
    rslt = greet()
    print(rslt + "\n")
