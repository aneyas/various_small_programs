# The naive solution.
def fib1(n: int) -> int:
    if n < 2:
        return n
    return fib1(n-1) + fib1(n-2)

# Using functool to cache the results.
from functools import lru_cache
@lru_cache(maxsize = None)
def fib4(n:int) -> int:
    if n < 2:
        return n
    return fib4(n - 1) + fib4(n - 2)


# Using dict as a memo.
from typing import Dict
memo: Dict[int,int] = {0:0, 1:1}

def fib3(n:int) -> int:
    if n not in memo:
        memo[n] = fib3(n-1) + fib3(n-2)
    return memo[n]

# The iterative version, most performant.
def fib5(n:int)->int:
    if n == 0: return n
    last: int = 0
    next: int = 1
    for _ in range(1,n):
        last, next = next, last + next
    return next

# Using generator
from typing import Generator
def fib6(n:int) -> Generator[int, None, None]:
    yield 0
    if n > 0: yield 1
    last: int = 0
    next: int = 1
    for _ in range(1,n):
        last, next = next, last + next
        yield next

if __name__ == "__main__":
    print(fib1(5))
    print(fib1(10))
    print(fib3(5))
    print(fib3(50))
    print(fib4(50))
    print(fib5(50))
    for i in fib6(50):
        print(i)
