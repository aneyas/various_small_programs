;(require-extension sicp)

(define (make-accumulator acc)
  (lambda (v)
    (begin
      (set! acc (+ acc v))
      acc)))

;Do some test.

(define A (make-accumulator 5))

(print (A 10))
(print (A 10))

(define B (make-accumulator 3))

(print (B 10))
(print (B 10))



