(require-extension sicp)

;;ex: 3-2
(define (make-monitored f)
  (let ((count 0))
    (define (how-many-calls?)
      count)
    (define (reset-count)
      (set! count 0))
    (define (mf m)
      (cond ((eq? m 'how-many-calls?) (how-many-calls?))
            ((eq? m 'reset-count) (reset-count))
            (else (begin
                    (set! count (+ count 1))
                    (f m)))))
    mf))


;;ex: 3-3
(define (make-account password balance)
  (define (withdraw amount)
    (if (>= balance amount)
      (begin (set! balance (- balance amount))
             balance)
      "Insufficient funds"
      ))
  (define (deposit amount)
    (set! balance (+ balance amount))
    balance)

  (define (dispatch given-password m)
    (if (eq? given-password password)
      (begin
        (cond ((eq? m 'withdraw) withdraw)
              ((eq? m 'deposit) deposit)
              (else (error "Unknown request -- MAKE-ACCOUNT" m))))
      (error "Incorrect password"))
    )

  dispatch)

;;ex: 3-4
(define (make-account2 password balance)
  (let ((password-tried 0))
    (define (withdraw amount)
      (if (>= balance amount)
        (begin (set! balance (- balance amount))
               balance)
        "Insufficient funds"))
    (define (deposit amount)
      (set! balance (+ balance amount))
      balance)

    (define (incorrect-password amount)
      (begin (set! password-tried (+ password-tried 1))
             (print "Passord tried " password-tried " times")))

    (define (dispatch given-password m)
      (if (eq? given-password password)
        (begin
          (cond ((eq? m 'withdraw) withdraw)
                ((eq? m 'deposit) deposit)
                (else (error "Unknown request -- MAKE-ACCOUNT" m))))
        (if (< password-tried 6) incorrect-password
          (error "Call cops"))))
    dispatch))
