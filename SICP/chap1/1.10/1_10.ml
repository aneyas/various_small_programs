let-rec A x y =
  match (x,y) with
  |(_,y) -> 0
  |(0,_) -> 2 * y
  |(_,1) -> 2
  |(_,_) -> A (x - 1) (A x (y-1))

let f n = A 0 n

let g n = A 1 n

let h n = A 2 n

let x = A 1 10

print_int x; print_endline;;


