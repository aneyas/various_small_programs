let rec a x y =
  match (x,y) with
   (_,0) -> 0
  |(0,_) -> 2 * y
  |(_,1) -> 2
  |(_,_) -> a (x-1) (a x (y-1))

let f n = a 0 n

let g n = a 1 n

let h n = a 2 n

let x = a 1 10;;
print_int x; print_endline "";;

let y = a 2 4;;
print_int y; print_endline "";;

let z = h 4;;
print_int z; print_endline "";;

let z1 = a 2 3;;
print_int z1; print_endline "";;

let z2 = a 3 3;;
print_int z2; print_endline "";;
