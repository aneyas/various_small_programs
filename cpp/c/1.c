#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <limits.h>
#include <stdint.h>

#define MAX_UINT32 0xFFFFFFFF

int
main(){
    printf("%u",MAX_UINT32);
    return 0;
}
