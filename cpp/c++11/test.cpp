/*
 * =====================================================================================
 *
 *       Filename:  test.cpp
 *
 *    Description:  Test c++11 compiler.
 *
 *        Version:  1.0
 *        Created:  08/15/2014 11:47:08
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */

#include <array>
#include <iostream>

using namespace std;

int main()
{
    std::array<int, 3> arr = {2, 3, 5};

    int *i = new int;
    auto_ptr<int> x(i);
    auto_ptr<int> y;

    y = x;

    cout << x.get() << endl; // Print NULL
    cout << y.get() << endl; // Print non-NULL address i
    cout << *y << endl;
    *i = 10;
    cout << *y << endl;

    const int n = 20;
    array<int, n> arr2;
}
