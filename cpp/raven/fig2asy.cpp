#include<iostream>
#include<cstdio>
#include<cstdlib>
#include<cassert>
#include<cctype>

using namespace std;

enum move {INIT=0, START=1, NUM=5, BIG=7, 
           JUMP=3, DRAW_LINE=4,    STOP=2, PAUSE=8};

/*#define INIT 0
#define START 1
#define NUM 5
#define BIG 7
#define JUMP 3
#define DRAW_LINE 4
#define STOP 2
#define PAUSE 8
*/

void
start_asyfile(){
  cout << "size(1200,1200);" << endl;
  cout << "pen p=linetype(\"\")+3*linewidth(currentpen);" << endl;

  cout << "pen p1=linetype(\"\")+0*linewidth(currentpen);" << endl;
  cout << "pen p2=linetype(\"\")+20*linewidth(currentpen);" << endl;
  cout << "draw(circle((0,0),0),p=p1);" << endl;
  cout << "draw(circle((1400,0),0),p=p1);" << endl;
  cout << "draw(circle((0,900),0),p=p1);" << endl;

}

void
complete_asyfile(){
}


void
find_shifts(int fig_num, int &shift_x, int &shift_y){
  assert(fig_num>=1);
  assert(fig_num<=10);

  if(fig_num == 7){ //This is the big figure.
    shift_x = 100;
    shift_y = 400;
    return;
  }else if(fig_num >=9){
    fig_num -= 2;  //index 7,8 is not used, so figures
                   //are numbered as 1,2...6,9,10.
  }

  //Now figure out how to do the shift.
  //
  //For now, I use 4 rows, 2 cols to host the figures.

  int nrow, ncol;
  nrow = (fig_num - 1) / 2; //0,1
  ncol = (fig_num - 1) % 2; //0,1,2,3

  shift_x = 800 + ncol * 320;
  shift_y = 800 - (100 + nrow * 200);
}

void
jump_to(int &ox,int &oy, int &x, int &y){
  ox = x;
  oy = y;
}


void
draw_line(int &ox,int &oy,int &x,int &y,int fig_num){
  int shift_x, shift_y;
  find_shifts(fig_num, shift_x, shift_y);

  printf("draw((%d,%d)--(%d,%d),p=p);\n", ox+shift_x, oy+shift_y, x+shift_x, y+shift_y);
  jump_to(ox,oy,x,y);
}

void
draw_label(int fig_num){
  if(fig_num == 7) return;
  int shift_x, shift_y;
  find_shifts(fig_num, shift_x, shift_y);

  if(fig_num >=9) fig_num -=2;

  printf("Label L = Label(\"(%d)\");\n", fig_num);
  printf("label(L= scale(2.5)*L,(%d,%d),SW,p=p2);\n", shift_x, shift_y);
}

void
eat_OP(){
  int temp;
  char c;

  c = cin.get();
  while(!isdigit(c)){
    c = cin.get();
  }

  cin.unget();
  cin >> temp;
  return;
}

void
read_OP(int &OP){
  //Do peek to remove all the space or other symbols.
  char c;

  c = cin.get();
  while(!isdigit(c)){
    c = cin.get();
  }

  cin.unget();
  cin >> OP;
  return;
}

int
main(){
  int this_move;
  this_move = INIT;

  int fig_num;

  int ox, oy; // Origin x,y.
  int x,y;
  bool is_new_fig = false;

  start_asyfile();

  while(this_move!=STOP){
    switch(this_move){
      case INIT: case PAUSE:
        break;
      case START:
        eat_OP(); //Remove the OP.
        break;
      case NUM:
        read_OP(fig_num);
        if(fig_num==7){ //The big figure.
          eat_OP();
          eat_OP(); //eat 7,0 which seems to be useless.
        }
        is_new_fig = true;
        break;
      case JUMP:
        read_OP(x);
        read_OP(y);
        jump_to(ox,oy,x,y);
        break;
      case DRAW_LINE:
        read_OP(x);
        read_OP(y);
        draw_line(ox,oy,x,y,fig_num);
        if(is_new_fig){
          draw_label(fig_num);
          is_new_fig = false;
        }
        break;
      default:
        cout<<"Wrong OP in file!"<<endl;
        exit(11);
    }
    read_OP(this_move);
  }

  complete_asyfile();
  return 0;
}

