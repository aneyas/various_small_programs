#!/bin/bash

for thing in $@
do
  filename=`basename ${thing}`
  echo ${filename}
  afile=${filename%.FIG}
  ofile=${filename%.FIG}".asy"
  ./a.out < ${thing} > OUT/${ofile};
  cd OUT
  echo "Label L2 = Label(\"$afile\");" >> $ofile
  echo "label(L= scale(5)*L2,(40,40),NE,p=p2);" >> $ofile
  asy ${ofile};
  cd ..
done  
